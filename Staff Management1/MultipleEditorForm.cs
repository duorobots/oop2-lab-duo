﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Staff_Management1
{
    public partial class MultipleEditorForm : Form
    {
        List<Multiple> multipleDbList;
        int selectedType = -1;
        int selectedObject = -1;

        public MultipleEditorForm()
        {
            InitializeComponent();
        }

        // Overloaded Constructor to Get Address of Created Objects from MainForm - Start
        public MultipleEditorForm(List<Multiple> x)
        {
            InitializeComponent();
            multipleDbList = x;
        }
        // Overloaded Constructor to Get Address of Created Objects from MainForm - End

        // Load Multiples Database And New UI - Start
        private void MultipleEditorForm_Load(object sender, EventArgs e)
        {
            // New UI - Start
            cmboxType.Items.Clear();

            cmboxType.Items.Add("Experience");
            cmboxType.Items.Add("City");
            cmboxType.Items.Add("Education");
            cmboxType.Items.Add("Foreign Language Knowledge");
            cmboxType.Items.Add("Job");
            cmboxType.Items.Add("Family Status");
            cmboxType.Items.Add("BMO Salary");
            // New UI - End

            // Load Multiples Database - Start
            int errorCheck = 0;
            string path = @"C:\Users\Kartal\Desktop\MultiplesTest.txt";

            if (File.Exists(path))
            {
                string loadRes = File.ReadAllText(path);
                loadRes = loadRes.Trim();

                if (loadRes != "")
                {
                    string[] lines = loadRes.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    double[] experienceX = { 0, 0, 0, 0, 0 };
                    double[] cityX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    double[] educationX = { 0, 0, 0, 0, 0 };
                    double[] foreignLanguageKnowledgeX = { 0 };
                    double[] jobX = { 0, 0, 0, 0, 0, 0 };
                    double[] familyStatusX = { 0, 0, 0, 0 };
                    double BMOSalaryX = 0;

                    // Experience
                    try
                    {
                        experienceX = lines[0].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheck = -1;
                    }

                    // City
                    try
                    {
                        cityX = lines[1].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheck = -1;
                    }

                    // Education
                    try
                    {
                        educationX = lines[2].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheck = -1;
                    }

                    // Foreign Language Knowledge
                    try
                    {
                        foreignLanguageKnowledgeX = lines[3].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheck = -1;
                    }

                    // Job
                    try
                    {
                        jobX = lines[4].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheck = -1;
                    }

                    // Family Status
                    try
                    {
                        familyStatusX = lines[5].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheck = -1;
                    }

                    // BMO Salary
                    try
                    {
                        BMOSalaryX = Double.Parse(lines[6]);
                    }
                    catch
                    {
                        errorCheck = -1;
                    }
                   
                    if(errorCheck==0)
                    {
                        multipleDbList.Add(new Multiple
                        {
                            experience = experienceX,
                            city = cityX,
                            education = educationX,
                            foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                            job = jobX,
                            familyStatus = familyStatusX,
                            BMOSalary = BMOSalaryX
                        });
                    }
                    else
                    {
                        multipleDbList.Add(new Multiple
                        {
                            experience = experienceX,
                            city = cityX,
                            education = educationX,
                            foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                            job = jobX,
                            familyStatus = familyStatusX,
                            BMOSalary = BMOSalaryX
                        });
                        MessageBox.Show("'Multiple' Load File Is Wrong...All Multiples Set To '0'");
                    }
                }
                else
                {
                    double[] experienceX = { 0, 0, 0, 0, 0 };
                    double[] cityX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    double[] educationX = { 0, 0, 0, 0, 0 };
                    double[] foreignLanguageKnowledgeX = { 0, 0, 0 };
                    double[] jobX = { 0, 0, 0, 0, 0, 0 };
                    double[] familyStatusX = { 0, 0, 0, 0 };
                    double BMOSalaryX = 0;

                    multipleDbList.Add(new Multiple
                    {
                        experience = experienceX,
                        city = cityX,
                        education = educationX,
                        foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                        job = jobX,
                        familyStatus = familyStatusX,
                        BMOSalary = BMOSalaryX
                    });
                    MessageBox.Show("'Multiple' Load File Is Empty...All Multiples Set To '0'");
                }
            }
            else // If File Doesnt Exists Set 0 as Default
            {
                double[] experienceX = { 0, 0, 0, 0, 0 };
                double[] cityX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                double[] educationX = { 0, 0, 0, 0, 0 };
                double[] foreignLanguageKnowledgeX = { 0, 0, 0 };
                double[] jobX = { 0, 0, 0, 0, 0, 0 };
                double[] familyStatusX = { 0, 0, 0, 0 };
                double BMOSalaryX = 0;

                multipleDbList.Add(new Multiple
                {
                    experience = experienceX,
                    city = cityX,
                    education = educationX,
                    foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                    job = jobX,
                    familyStatus = familyStatusX,
                    BMOSalary = BMOSalaryX
                });
                MessageBox.Show("'Multiple' Load File Doesnt Exists...All Multiples Set To '0'");
            }
            // Load Multiples Database - End
        }
        // Load Multiples Database And New UI - End

        // Type Select Changed - Start
        private void cmboxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedType = cmboxType.SelectedIndex;
            selectedObject = -1;
            txtValue.Text = "";

            // Experience
            if (cmboxType.SelectedIndex == 0)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("2 - 4");
                cmboxObject.Items.Add("5 - 9");
                cmboxObject.Items.Add("10 - 14");
                cmboxObject.Items.Add("15 - 20");
                cmboxObject.Items.Add("20+");
            }
            // City
            else if (cmboxType.SelectedIndex == 1)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("Adana");
                cmboxObject.Items.Add("Adıyaman");
                cmboxObject.Items.Add("Afyonkarahisar");
                cmboxObject.Items.Add("Ağrı");
                cmboxObject.Items.Add("Aksaray");
                cmboxObject.Items.Add("Amasya");
                cmboxObject.Items.Add("Ankara");
                cmboxObject.Items.Add("Antalya");
                cmboxObject.Items.Add("Ardahan");
                cmboxObject.Items.Add("Artvin");
                cmboxObject.Items.Add("Aydın");
                cmboxObject.Items.Add("Balıkesir");
                cmboxObject.Items.Add("Bartın");
                cmboxObject.Items.Add("Batman");
                cmboxObject.Items.Add("Bayburt");
                cmboxObject.Items.Add("Bilecik");
                cmboxObject.Items.Add("Bingöl");
                cmboxObject.Items.Add("Bitlis");
                cmboxObject.Items.Add("Bolu");
                cmboxObject.Items.Add("Burdur");
                cmboxObject.Items.Add("Bursa");
                cmboxObject.Items.Add("Çanakkale");
                cmboxObject.Items.Add("Çankırı");
                cmboxObject.Items.Add("Çorum");
                cmboxObject.Items.Add("Denizli");
                cmboxObject.Items.Add("Diyarbakır");
                cmboxObject.Items.Add("Düzce");
                cmboxObject.Items.Add("Edirne");
                cmboxObject.Items.Add("Elazığ");
                cmboxObject.Items.Add("Erzincan");
                cmboxObject.Items.Add("Erzurum");
                cmboxObject.Items.Add("Eskişehir");
                cmboxObject.Items.Add("Gaziantep");
                cmboxObject.Items.Add("Giresun");
                cmboxObject.Items.Add("Gümüşhane");
                cmboxObject.Items.Add("Hakkari");
                cmboxObject.Items.Add("Hatay");
                cmboxObject.Items.Add("Iğdır");
                cmboxObject.Items.Add("Isparta");
                cmboxObject.Items.Add("İstanbul");
                cmboxObject.Items.Add("İzmir");
                cmboxObject.Items.Add("Kahramanmaraş");
                cmboxObject.Items.Add("Karabük");
                cmboxObject.Items.Add("Karaman");
                cmboxObject.Items.Add("Kars");
                cmboxObject.Items.Add("Kastamonu");
                cmboxObject.Items.Add("Kayseri");
                cmboxObject.Items.Add("Kırıkkale");
                cmboxObject.Items.Add("Kırklareli");
                cmboxObject.Items.Add("Kırşehir");
                cmboxObject.Items.Add("Kilis");
                cmboxObject.Items.Add("Kocaeli");
                cmboxObject.Items.Add("Konya");
                cmboxObject.Items.Add("Kütahya");
                cmboxObject.Items.Add("Malatya");
                cmboxObject.Items.Add("Manisa");
                cmboxObject.Items.Add("Mardin");
                cmboxObject.Items.Add("Mersin");
                cmboxObject.Items.Add("Muğla");
                cmboxObject.Items.Add("Muş");
                cmboxObject.Items.Add("Nevşehir");
                cmboxObject.Items.Add("Niğde");
                cmboxObject.Items.Add("Ordu");
                cmboxObject.Items.Add("Osmaniye");
                cmboxObject.Items.Add("Rize");
                cmboxObject.Items.Add("Sakarya");
                cmboxObject.Items.Add("Samsun");
                cmboxObject.Items.Add("Siirt");
                cmboxObject.Items.Add("Sinop");
                cmboxObject.Items.Add("Sivas");
                cmboxObject.Items.Add("Şanlıurfa");
                cmboxObject.Items.Add("Şırnak");
                cmboxObject.Items.Add("Tekirdağ");
                cmboxObject.Items.Add("Tokat");
                cmboxObject.Items.Add("Trabzon");
                cmboxObject.Items.Add("Tunceli");
                cmboxObject.Items.Add("Uşak");
                cmboxObject.Items.Add("Van");
                cmboxObject.Items.Add("Yalova");
                cmboxObject.Items.Add("Yozgat");
                cmboxObject.Items.Add("Zonguldak");
            }
            // Education
            else if (cmboxType.SelectedIndex == 2)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("Meslek Alanda Yüksek Lisans");
                cmboxObject.Items.Add("Meslek Alanda Doktora");
                cmboxObject.Items.Add("Meslek Alanda Doçentlik");
                cmboxObject.Items.Add("Meslek Alan Harici Yüksek Lisans");
                cmboxObject.Items.Add("Meslek Alan Harici Doktora / Doçentlik");
            }
            // Foreign Language Knowledge
            else if (cmboxType.SelectedIndex == 3)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("Belgelendirilmiş İngilizce Bilgisi");
                cmboxObject.Items.Add("İngilizce Eğitim Veren Okul Mezuniyeti");
                cmboxObject.Items.Add("Belgelendirilmiş Diğer Yabancı Dil Bilgisi (Her Dil İçin)");
            }
            // Job
            else if (cmboxType.SelectedIndex == 4)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("Takım Lideri / Grup Yöneticisi / Teknik Yönetici / Yazılım Mimarı");
                cmboxObject.Items.Add("Proje Yöneticisi");
                cmboxObject.Items.Add("Direktör / Projeler Yöneticisi");
                cmboxObject.Items.Add("CTO/Genel Müdür");
                cmboxObject.Items.Add("Bilgi İşlem Sorumlusu / Müdürü (Bilgi İşlem Biriminde En Çok 5 Bilişim Personeli Varsa)");
                cmboxObject.Items.Add("Bilgi İşlem Sorumlusu / Müdürü (Bilgi İşlem Biriminde 5'ten Çok Bilişim Personeli Varsa)");
            }
            // Family Status
            else if (cmboxType.SelectedIndex == 5)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("Evli ve Eşi Çalışmıyor");
                cmboxObject.Items.Add("0-6 Yaş Arası Çocuk");
                cmboxObject.Items.Add("7-18 Yaş Arası Çocuk");
                cmboxObject.Items.Add("18 Yaş Üstü Çocuk (Üniversite Lisans / Ön Lisans Öğrencisi Olmak Koşuluyla)");
            }
            // BMO Salary
            else if (cmboxType.SelectedIndex == 6)
            {
                cmboxObject.Items.Clear();

                cmboxObject.Items.Add("BMO Salary");
            }
        }
        // Type Select Changed - End

        // Object Select Changed - Start
        private void cmboxObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedObject = cmboxObject.SelectedIndex;

            // Experience
            if (selectedType == 0)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].experience[selectedObject].ToString("F");
                }
            }
            // City
            else if (selectedType == 1)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].city[selectedObject].ToString("F");
                }
            }
            // Education
            else if (selectedType == 2)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].education[selectedObject].ToString("F");
                }
            }
            // Foreign Language Knowledge
            else if (selectedType == 3)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].foreignLanguageKnowledge[selectedObject].ToString("F");
                }
            }
            // Job
            else if (selectedType == 4)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].job[selectedObject].ToString("F");
                }
            }
            // Family Status
            else if (selectedType == 5)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].familyStatus[selectedObject].ToString("F");
                }
            }
            // BMO Salary
            else if (selectedType == 6)
            {
                if (selectedObject != -1)
                {
                    txtValue.Text = multipleDbList[0].BMOSalary.ToString("F");
                }
            }
        }
        // Object Select Changed - End

        // Update Multiple at Database - Start
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // Experience
            if (selectedType == 0)
            {
                if(selectedObject!=-1)
                {
                    multipleDbList[0].experience[selectedObject] = Double.Parse(txtValue.Text);
                }
            }
            // City
            else if (selectedType == 1)
            {
                if (selectedObject != -1)
                {
                    multipleDbList[0].city[selectedObject] = Double.Parse(txtValue.Text);
                }
            }
            // Education
            else if (selectedType == 2)
            {
                if (selectedObject != -1)
                {
                    multipleDbList[0].education[selectedObject] = Double.Parse(txtValue.Text);
                }
            }
            // Foreign Language Knowledge
            else if (selectedType == 3)
            {
                if (selectedObject != -1)
                {
                    multipleDbList[0].foreignLanguageKnowledge[selectedObject] = Double.Parse(txtValue.Text);
                }
            }
            // Job
            else if (selectedType == 4)
            {
                if (selectedObject != -1)
                {
                    multipleDbList[0].job[selectedObject] = Double.Parse(txtValue.Text);
                }
            }
            // Family Status
            else if (selectedType == 5)
            {
                if (selectedObject != -1)
                {
                    multipleDbList[0].familyStatus[selectedObject] = Double.Parse(txtValue.Text);
                }
            }
            // BMO Salary
            else if (selectedType == 6)
            {
                if (selectedObject != -1)
                {
                    multipleDbList[0].BMOSalary = Double.Parse(txtValue.Text);
                }
            }

            MessageBox.Show("Multiple Updated...");
        }
        // Update Multiple at Database - End

        // Save Multiples to File - Start
        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            string path = @"C:\Users\Kartal\Desktop\MultiplesTest.txt";
            File.WriteAllText(path, listAllMultiples());
        }
        // Save Multiples to File - End

        // List All Multiples - Start
        public string listAllMultiples()
        {
            string res = "";
            char x = '\t';

            // Experience
            for (int i = 0; i < multipleDbList[0].experience.Length; i++)
            {
                res += multipleDbList[0].experience[i].ToString("F") + x;
            }
            res = res.Remove(res.Length - 1);
            res += Environment.NewLine;

            // City
            for (int i = 0; i < multipleDbList[0].city.Length; i++)
            {
                res += multipleDbList[0].city[i].ToString("F") + x;
            }
            res = res.Remove(res.Length - 1);
            res += Environment.NewLine;

            // Education
            for (int i = 0; i < multipleDbList[0].education.Length; i++)
            {
                res += multipleDbList[0].education[i].ToString("F") + x;
            }
            res = res.Remove(res.Length - 1);
            res += Environment.NewLine;

            // Foreign Language Knowledge
            for (int i = 0; i < multipleDbList[0].foreignLanguageKnowledge.Length; i++)
            {
                res += multipleDbList[0].foreignLanguageKnowledge[i].ToString("F") + x;
            }
            res = res.Remove(res.Length - 1);
            res += Environment.NewLine;

            // Job
            for (int i = 0; i < multipleDbList[0].job.Length; i++)
            {
                res += multipleDbList[0].job[i].ToString("F") + x;
            }
            res = res.Remove(res.Length - 1);
            res += Environment.NewLine;

            // Family Status
            for (int i = 0; i < multipleDbList[0].familyStatus.Length; i++)
            {
                res += multipleDbList[0].familyStatus[i].ToString("F") + x;
            }
            res = res.Remove(res.Length - 1);
            res += Environment.NewLine;

            // BMO Salary
            res += multipleDbList[0].BMOSalary.ToString("F");

            return res;
        }
        // List All Multiples - End
    }
}
