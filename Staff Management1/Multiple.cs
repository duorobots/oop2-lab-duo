﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staff_Management1
{
    public class Multiple
    {
        public double[] experience { get; set; }
        public double[] city { get; set; }
        public double[] education { get; set; }
        public double[] foreignLanguageKnowledge { get; set; }
        public double[] job { get; set; }
        public double[] familyStatus { get; set; }
        public double BMOSalary { get; set; }
    }
}
