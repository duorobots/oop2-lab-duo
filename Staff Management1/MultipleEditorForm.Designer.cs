﻿namespace Staff_Management1
{
    partial class MultipleEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmboxType = new System.Windows.Forms.ComboBox();
            this.cmboxObject = new System.Windows.Forms.ComboBox();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSaveChanges = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmboxType
            // 
            this.cmboxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxType.FormattingEnabled = true;
            this.cmboxType.Location = new System.Drawing.Point(12, 12);
            this.cmboxType.Name = "cmboxType";
            this.cmboxType.Size = new System.Drawing.Size(200, 21);
            this.cmboxType.TabIndex = 0;
            this.cmboxType.SelectedIndexChanged += new System.EventHandler(this.cmboxType_SelectedIndexChanged);
            // 
            // cmboxObject
            // 
            this.cmboxObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxObject.FormattingEnabled = true;
            this.cmboxObject.Location = new System.Drawing.Point(12, 39);
            this.cmboxObject.Name = "cmboxObject";
            this.cmboxObject.Size = new System.Drawing.Size(420, 21);
            this.cmboxObject.TabIndex = 1;
            this.cmboxObject.SelectedIndexChanged += new System.EventHandler(this.cmboxObject_SelectedIndexChanged);
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(122, 66);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(200, 20);
            this.txtValue.TabIndex = 2;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(226, 10);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 23);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Location = new System.Drawing.Point(332, 10);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(100, 23);
            this.btnSaveChanges.TabIndex = 4;
            this.btnSaveChanges.Text = "Save Changes";
            this.btnSaveChanges.UseVisualStyleBackColor = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // MultipleEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 97);
            this.Controls.Add(this.btnSaveChanges);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.cmboxObject);
            this.Controls.Add(this.cmboxType);
            this.Name = "MultipleEditorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Multiple Editor";
            this.Load += new System.EventHandler(this.MultipleEditorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmboxType;
        private System.Windows.Forms.ComboBox cmboxObject;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSaveChanges;
    }
}