﻿namespace Staff_Management1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstviewStaff = new System.Windows.Forms.ListView();
            this.columnID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSurname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSalary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAddNewStaff = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.savefileStaffList = new System.Windows.Forms.SaveFileDialog();
            this.btnLoad = new System.Windows.Forms.Button();
            this.openfileStaffList = new System.Windows.Forms.OpenFileDialog();
            this.btnMultipleEditor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstviewStaff
            // 
            this.lstviewStaff.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnID,
            this.columnName,
            this.columnSurname,
            this.columnSalary});
            this.lstviewStaff.FullRowSelect = true;
            this.lstviewStaff.GridLines = true;
            this.lstviewStaff.Location = new System.Drawing.Point(12, 68);
            this.lstviewStaff.Name = "lstviewStaff";
            this.lstviewStaff.Size = new System.Drawing.Size(750, 450);
            this.lstviewStaff.TabIndex = 0;
            this.lstviewStaff.UseCompatibleStateImageBehavior = false;
            this.lstviewStaff.View = System.Windows.Forms.View.Details;
            this.lstviewStaff.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstviewStaff_ColumnClick);
            this.lstviewStaff.DoubleClick += new System.EventHandler(this.lstviewStaff_DoubleClick);
            // 
            // columnID
            // 
            this.columnID.Text = "ID";
            this.columnID.Width = 65;
            // 
            // columnName
            // 
            this.columnName.Text = "Name";
            this.columnName.Width = 150;
            // 
            // columnSurname
            // 
            this.columnSurname.Text = "Surname";
            this.columnSurname.Width = 150;
            // 
            // columnSalary
            // 
            this.columnSalary.Text = "Salary";
            this.columnSalary.Width = 150;
            // 
            // btnAddNewStaff
            // 
            this.btnAddNewStaff.Location = new System.Drawing.Point(662, 12);
            this.btnAddNewStaff.Name = "btnAddNewStaff";
            this.btnAddNewStaff.Size = new System.Drawing.Size(100, 50);
            this.btnAddNewStaff.TabIndex = 1;
            this.btnAddNewStaff.Text = "Add New Staff";
            this.btnAddNewStaff.UseVisualStyleBackColor = true;
            this.btnAddNewStaff.Click += new System.EventHandler(this.btnAddNewStaff_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 50);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(118, 12);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(100, 50);
            this.btnLoad.TabIndex = 3;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnMultipleEditor
            // 
            this.btnMultipleEditor.Location = new System.Drawing.Point(224, 12);
            this.btnMultipleEditor.Name = "btnMultipleEditor";
            this.btnMultipleEditor.Size = new System.Drawing.Size(100, 50);
            this.btnMultipleEditor.TabIndex = 4;
            this.btnMultipleEditor.Text = "Multiple Editor";
            this.btnMultipleEditor.UseVisualStyleBackColor = true;
            this.btnMultipleEditor.Click += new System.EventHandler(this.btnMultipleEditor_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 530);
            this.Controls.Add(this.btnMultipleEditor);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAddNewStaff);
            this.Controls.Add(this.lstviewStaff);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstviewStaff;
        private System.Windows.Forms.ColumnHeader columnID;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnSurname;
        private System.Windows.Forms.ColumnHeader columnSalary;
        private System.Windows.Forms.Button btnAddNewStaff;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SaveFileDialog savefileStaffList;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.OpenFileDialog openfileStaffList;
        private System.Windows.Forms.Button btnMultipleEditor;
    }
}