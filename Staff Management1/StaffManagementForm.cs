﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Staff_Management1
{
    public partial class StaffManagementForm : Form
    {
        StaffListControl staffListControl;
        MainForm mainForm;
        int staffIndex = -1;

        public StaffManagementForm()
        {
            InitializeComponent();
        }

        // Overloaded Constructor to Get Address of Created Objects from MainForm - Start
        public StaffManagementForm(StaffListControl x, MainForm y)
        {
            InitializeComponent();
            staffListControl = x;
            mainForm = y;
        }
        // Overloaded Constructor to Get Address of Created Objects from MainForm - End

        // Staff Index Get/Set - Start
        public int StaffIndex
        {
            get { return staffIndex; }
            set { staffIndex = value; }
        }
        // Staff Index Get/Set - End

        // UI Load - Start
        private void StaffManagementForm_Load(object sender, EventArgs e)
        {
            if (staffIndex != -1)
            {
                databaseStaffUI();
            }
            else
            {
                newStaffUI();
            }
        }
        // UI Load - End

        // Set UI to New Staff - Start
        public void newStaffUI()
        {
            lblBMOSalary.Visible = false;
            btnUpdate.Visible = false;
            btnDelete.Visible = false;
            picboxStaffAvatar.Image = null;
            txtName.Text = "";
            txtSurname.Text = "";
            cmboxHomeCity.SelectedIndex = -1;
            cmboxWorkCity.SelectedIndex = -1;
            cmboxEducation.SelectedIndex = -1;
            cmboxForeingLK.SelectedIndex = -1;
            chckboxOtherLanguages.Checked = false;
            txtOtherLanguages.Text = "";
            cmboxJob.SelectedIndex = -1;
            txtJobNotListed.Text = "";
            txtExperience.Text = "";
            rdbtnSingle.Checked = false;
            rdbtnMarried.Checked = false;
            rdbtnWorking.Checked = false;
            rdbtnNotWorking.Checked = false;
            cmboxKidCount.SelectedIndex = -1;
            txtFirstKidsBDay.Text = "";
            rdbtnStudentFirst.Checked = false;
            rdbtnNotStudentFirst.Checked = false;
            txtSecondKidsBDay.Text = "";
            rdbtnStudentSecond.Checked = false;
            rdbtnNotStudentSecond.Checked = false;
            txtSalary.Text = "";

            // Other Languages
            lblOtherLanguages1.Visible = false;
            lblOtherLanguages2.Visible = false;
            txtOtherLanguages.Visible = false;

            //Job Not Listed
            lblJobNotListed.Visible = false;
            txtJobNotListed.Visible = false;

            // Partners Job Status
            lblPartnersJobStatus.Visible = false;
            grpboxPartnersJob.Visible = false;

            // First Kids Birthday
            lblFirstKidsBDay.Visible = false;
            txtFirstKidsBDay.Visible = false;

            // First Kids Education Status
            lblFirstKidsEducation.Visible = false;
            grpboxKidsEducationFirst.Visible = false;

            // Second Kids Birthday
            lblSecondKidsBDay.Visible = false;
            txtSecondKidsBDay.Visible = false;

            // Second Kids Education Status
            lblSecondKidsEducation.Visible = false;
            grpboxKidsEducationSecond.Visible = false;

            //Work Type
            rdbtnFull.Checked = false;
            rdbtnPart.Checked = false;
        }
        // Set UI to New Staff - End

        // Set UI to Staff From Database - Start
        public void databaseStaffUI()
        {
            lblBMOSalary.Text = "BMO Salary: " + staffListControl.calculateBMO(staffIndex).ToString();

            btnAdd.Visible = false;

            string tmpRes = staffListControl.bringTheStaff(staffIndex);
            string[] tmpElements = tmpRes.Split(',');

            txtName.Text = tmpElements[1];
            txtSurname.Text = tmpElements[2];
            cmboxHomeCity.SelectedIndex = Int32.Parse(tmpElements[3]);
            cmboxWorkCity.SelectedIndex = Int32.Parse(tmpElements[4]);
            cmboxEducation.SelectedIndex = Int32.Parse(tmpElements[5]);
            cmboxForeingLK.SelectedIndex = Int32.Parse(tmpElements[6]);

            if (tmpElements[7] != "-")
            {
                chckboxOtherLanguages.Checked = true;
                txtOtherLanguages.Text = tmpElements[7];
                lblOtherLanguages1.Visible = true;
                lblOtherLanguages2.Visible = true;
                txtOtherLanguages.Visible = true;
            }
            else
            {
                chckboxOtherLanguages.Checked = false;
                txtOtherLanguages.Text = "";
                lblOtherLanguages1.Visible = false;
                lblOtherLanguages2.Visible = false;
                txtOtherLanguages.Visible = false;
            }

            cmboxJob.SelectedIndex = Int32.Parse(tmpElements[8]);

            if (tmpElements[9] != "-")
            {
                txtJobNotListed.Text = tmpElements[9];
                lblJobNotListed.Visible = true;
                txtJobNotListed.Visible = true;
            }
            else
            {
                txtJobNotListed.Text = "";
                lblJobNotListed.Visible = false;
                txtJobNotListed.Visible = false;
            }

            // tmpElements[10] == hiringDate --> O yuzden bu elemani pass.

            txtExperience.Text = tmpElements[11];

            if (tmpElements[12] != "False")
            {
                rdbtnSingle.Checked = false;
                rdbtnMarried.Checked = true;
            }
            else
            {
                rdbtnSingle.Checked = true;
                rdbtnMarried.Checked = false;
            }

            if (tmpElements[13] != "-1")
            {
                if (tmpElements[13] != "0")
                {
                    rdbtnWorking.Checked = true;
                    rdbtnNotWorking.Checked = false;
                }
                else
                {
                    rdbtnWorking.Checked = false;
                    rdbtnNotWorking.Checked = true;
                }

                lblPartnersJobStatus.Visible = true;
                grpboxPartnersJob.Visible = true;
            }
            else
            {
                rdbtnWorking.Checked = false;
                rdbtnNotWorking.Checked = false;

                lblPartnersJobStatus.Visible = false;
                grpboxPartnersJob.Visible = false;
            }

            cmboxKidCount.SelectedIndex = Int32.Parse(tmpElements[14]);

            if (tmpElements[15] != "-")
            {
                string[] tmpBDays = tmpElements[15].Split('/');
                if (tmpBDays.Length > 1)
                {
                    txtFirstKidsBDay.Text = tmpBDays[0];
                    txtSecondKidsBDay.Text = tmpBDays[1];

                    lblFirstKidsBDay.Visible = true;
                    txtFirstKidsBDay.Visible = true;

                    lblSecondKidsBDay.Visible = true;
                    txtSecondKidsBDay.Visible = true;
                }
                else
                {
                    txtFirstKidsBDay.Text = tmpBDays[0];

                    lblFirstKidsBDay.Visible = true;
                    txtFirstKidsBDay.Visible = true;

                    lblSecondKidsBDay.Visible = false;
                    txtSecondKidsBDay.Visible = false;
                }
            }
            else
            {
                txtFirstKidsBDay.Text = "";
                txtSecondKidsBDay.Text = "";

                lblFirstKidsBDay.Visible = false;
                txtFirstKidsBDay.Visible = false;

                lblSecondKidsBDay.Visible = false;
                txtSecondKidsBDay.Visible = false;
            }

            if (tmpElements[16] != "-1")
            {
                string[] tmpKidsEducationStatus = tmpElements[16].Split('/');
                if (tmpKidsEducationStatus.Length > 1)
                {
                    if (tmpKidsEducationStatus[0] != "1")
                    {
                        rdbtnStudentFirst.Checked = false;
                        rdbtnNotStudentFirst.Checked = true;
                    }
                    else
                    {
                        rdbtnStudentFirst.Checked = true;
                        rdbtnNotStudentFirst.Checked = false;
                    }

                    if (tmpKidsEducationStatus[1] != "1")
                    {
                        rdbtnStudentSecond.Checked = false;
                        rdbtnNotStudentSecond.Checked = true;
                    }
                    else
                    {
                        rdbtnStudentSecond.Checked = true;
                        rdbtnNotStudentSecond.Checked = false;
                    }

                    lblFirstKidsEducation.Visible = true;
                    grpboxKidsEducationFirst.Visible = true;

                    lblSecondKidsEducation.Visible = true;
                    grpboxKidsEducationSecond.Visible = true;
                }
                else
                {
                    if (tmpKidsEducationStatus[0] != "1")
                    {
                        rdbtnStudentFirst.Checked = false;
                        rdbtnNotStudentFirst.Checked = true;
                    }
                    else
                    {
                        rdbtnStudentFirst.Checked = true;
                        rdbtnNotStudentFirst.Checked = false;
                    }
                    rdbtnStudentSecond.Checked = false;
                    rdbtnNotStudentSecond.Checked = false;

                    lblFirstKidsEducation.Visible = true;
                    grpboxKidsEducationFirst.Visible = true;

                    lblSecondKidsEducation.Visible = false;
                    grpboxKidsEducationSecond.Visible = false;
                }
            }
            else
            {
                rdbtnStudentFirst.Checked = false;
                rdbtnNotStudentFirst.Checked = false;

                rdbtnStudentSecond.Checked = false;
                rdbtnNotStudentSecond.Checked = false;

                lblFirstKidsEducation.Visible = false;
                grpboxKidsEducationFirst.Visible = false;
                lblSecondKidsEducation.Visible = false;
                grpboxKidsEducationSecond.Visible = false;
            }

            txtSalary.Text = tmpElements[17];

            if (tmpElements[18] != "False\r\n")
            {
                rdbtnFull.Checked = true;
                rdbtnPart.Checked = false;
            }
            else
            {
                rdbtnFull.Checked = false;
                rdbtnPart.Checked = true;
            }

            int idX = staffListControl.staffIDofSelectedIndex(staffIndex);
            if (File.Exists(@"C:\Users\Kartal\Desktop\Test\" + idX.ToString() + ".jpg"))
            {
                picboxStaffAvatar.Image = Image.FromFile(@"C:\Users\Kartal\Desktop\Test\" + idX.ToString() + ".jpg");
            }
        }
        // Set UI to Staff From Database - End

        // Other Languages Selected or Not - Start
        private void chckboxOtherLanguages_CheckedChanged(object sender, EventArgs e)
        {
            if (chckboxOtherLanguages.Checked)
            {
                lblOtherLanguages1.Visible = true;
                lblOtherLanguages2.Visible = true;
                txtOtherLanguages.Visible = true;
            }
            else
            {
                txtOtherLanguages.Text = "";
                lblOtherLanguages1.Visible = false;
                lblOtherLanguages2.Visible = false;
                txtOtherLanguages.Visible = false;
            }
        }
        // Other Languages Selected or Not - End

        // Other Job Selected or Not - Start
        private void cmboxJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmboxJob.SelectedIndex == cmboxJob.Items.Count - 1)
            {
                lblJobNotListed.Visible = true;
                txtJobNotListed.Visible = true;
            }
            else
            {
                txtJobNotListed.Text = "";
                lblJobNotListed.Visible = false;
                txtJobNotListed.Visible = false;
            }
        }
        // Other Job Selected or Not - End

        // If Single - Start
        private void rdbtnSingle_Enter(object sender, EventArgs e)
        {
            rdbtnWorking.Checked = false;
            rdbtnNotWorking.Checked = false;
            lblPartnersJobStatus.Visible = false;
            grpboxPartnersJob.Visible = false;
        }
        // If Single - End

        // If Married - Start
        private void rdbtnMarried_Enter(object sender, EventArgs e)
        {
            lblPartnersJobStatus.Visible = true;
            grpboxPartnersJob.Visible = true;
        }
        // If Married - End

        // Kid Count - Start
        private void cmboxKidCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmboxKidCount.SelectedIndex == 0)
            {
                txtFirstKidsBDay.Text = "";
                rdbtnStudentFirst.Checked = false;
                rdbtnNotStudentFirst.Checked = false;

                txtSecondKidsBDay.Text = "";
                rdbtnStudentSecond.Checked = false;
                rdbtnNotStudentSecond.Checked = false;

                lblFirstKidsBDay.Visible = false;
                txtFirstKidsBDay.Visible = false;

                lblFirstKidsEducation.Visible = false;
                grpboxKidsEducationFirst.Visible = false;

                lblSecondKidsBDay.Visible = false;
                txtSecondKidsBDay.Visible = false;

                lblSecondKidsEducation.Visible = false;
                grpboxKidsEducationSecond.Visible = false;
            }
            else if (cmboxKidCount.SelectedIndex == 1)
            {
                txtSecondKidsBDay.Text = "";
                rdbtnStudentSecond.Checked = false;
                rdbtnNotStudentSecond.Checked = false;

                lblFirstKidsBDay.Visible = true;
                txtFirstKidsBDay.Visible = true;

                lblFirstKidsEducation.Visible = true;
                grpboxKidsEducationFirst.Visible = true;

                lblSecondKidsBDay.Visible = false;
                txtSecondKidsBDay.Visible = false;

                lblSecondKidsEducation.Visible = false;
                grpboxKidsEducationSecond.Visible = false;
            }
            else if (cmboxKidCount.SelectedIndex == 2 || cmboxKidCount.SelectedIndex == 3)
            {
                lblFirstKidsBDay.Visible = true;
                txtFirstKidsBDay.Visible = true;

                lblFirstKidsEducation.Visible = true;
                grpboxKidsEducationFirst.Visible = true;

                lblSecondKidsBDay.Visible = true;
                txtSecondKidsBDay.Visible = true;

                lblSecondKidsEducation.Visible = true;
                grpboxKidsEducationSecond.Visible = true;
            }
        }
        // Kid Count - End

        // Add Staff - Start
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int errorCheck = 0;
            string errorMessage = "";

            string nameX = "";
            string surnameX = "";
            string jobNotListedX = "-";
            string[] knownOtherLanguagesX = { "-" };
            string[] kidsBirthdaysX = { "-" };
            int homeCityX = -1;
            int workCityX = -1;
            int educationLevelX = -1;
            int foreignLanguageKnowledgeLevelX = -1;
            int jobLevelX = -1;
            int experienceAtHiringDateX = -1;
            int partnersJobStatusX = -1;
            int kidCountLevelX = -1;
            int[] kidsEducationStatusX = { -1 };
            double salaryX = 0;
            bool maritalStatusX = false;
            bool fullTimeX = false;

            string hiringDateX = DateTime.UtcNow.ToString("yyyy"); // Param #10 - hiringDate
            int idX = staffListControl.StaffID; // ID of Added Staff

            // Param #1 - Name
            try
            {
                nameX = txtName.Text; 

                if (nameX.Length < 1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Name' Input..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Name' Input..." + Environment.NewLine;
            }

            // Param #2 - Surname
            try
            {
                surnameX = txtSurname.Text; 

                if (surnameX.Length < 1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Surname' Input..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Surname' Input..." + Environment.NewLine;
            }

            // Param #3 - Home City
            try
            {
                homeCityX = cmboxHomeCity.SelectedIndex; 

                if(homeCityX==-1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Home City' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Home City' Choice..." + Environment.NewLine;
            }

            // Param #4 - Work City
            try
            {
                workCityX = cmboxWorkCity.SelectedIndex; 

                if(workCityX==-1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Work City' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Work City' Choice..." + Environment.NewLine;
            }

            // Param #5 - Education Level
            try
            {
                educationLevelX = cmboxEducation.SelectedIndex; 

                if(educationLevelX==-1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Education' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Education' Choice..." + Environment.NewLine;
            }

            // Param #6 - Foreign Language Knowledge Level
            try
            {
                foreignLanguageKnowledgeLevelX = cmboxForeingLK.SelectedIndex; 

                if(foreignLanguageKnowledgeLevelX==-1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Foreign Language Knowledge' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Foreign Language Knowledge' Choice..." + Environment.NewLine;
            }

            // Param #7 - Known Other Languages
            try
            {
                if(chckboxOtherLanguages.Checked)
                {
                    string tmpKnownOtherLanguages = txtOtherLanguages.Text;
                    if (tmpKnownOtherLanguages.Length > 0)
                    {
                        knownOtherLanguagesX = tmpKnownOtherLanguages.Split('/');
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Known Other Languages' Input..." + Environment.NewLine;
                    }
                }     
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Known Other Languages' Input..." + Environment.NewLine;
            }

            // Param #8 - Job Level
            try
            {
                jobLevelX = cmboxJob.SelectedIndex;  
                
                if(jobLevelX==-1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Job' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Job' Choice..." + Environment.NewLine;
            }

            // Param #9 - Job Not Listed
            try
            {
                if(jobLevelX==5)
                {
                    string tmpJobNotListed = txtJobNotListed.Text;

                    if (tmpJobNotListed.Length > 0)
                    {
                        jobNotListedX = tmpJobNotListed;
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Job Not Listed' Input..." + Environment.NewLine;
                    }
                }  
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Job Not Listed' Input..." + Environment.NewLine;
            }

            // Param #11 - Experience At Hiring Date
            try
            {
                experienceAtHiringDateX = Int32.Parse(txtExperience.Text); 
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Experience' Input..." + Environment.NewLine;
            }

            // Param #12 - Marital Status
            try
            {
                if (rdbtnMarried.Checked)
                {
                    maritalStatusX = true;
                }
                else if(rdbtnSingle.Checked)
                {
                    maritalStatusX = false;
                }
                else
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Marital Status' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Marital Status' Choice..." + Environment.NewLine;
            }

            // Param #13 - Partners Job Status
            try
            {
                if (maritalStatusX)
                {
                    if (rdbtnWorking.Checked)
                    {
                        partnersJobStatusX = 1;
                    }
                    else if(rdbtnNotWorking.Checked)
                    {
                        partnersJobStatusX = 0;
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Partners Job Status' Choice..." + Environment.NewLine;
                    }
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Partners Job Status' Choice..." + Environment.NewLine;
            }

            // Param #14 - Kid Count Level
            try
            {
                kidCountLevelX = cmboxKidCount.SelectedIndex; 

                if(kidCountLevelX==-1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Kid Count' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Kid Count' Choice..." + Environment.NewLine;
            }

            // Param #15 - Kids Birthdays
            try
            {
                string tmpKidsBirthdays = "-";
                
                if (kidCountLevelX == 2 || kidCountLevelX == 3)
                {
                    if(txtFirstKidsBDay.Text.Length<10)
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Birthdays' Input..." + Environment.NewLine;
                    }
                    else
                    {
                        tmpKidsBirthdays = txtFirstKidsBDay.Text + "/";
                    }

                    if (txtSecondKidsBDay.Text.Length < 10)
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Second Kid's Birthdays' Input..." + Environment.NewLine;
                    }
                    else
                    {
                        tmpKidsBirthdays += txtSecondKidsBDay.Text;
                    }
                }
                else if (kidCountLevelX == 1)
                {
                    if (txtFirstKidsBDay.Text.Length < 10)
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Birthdays' Input..." + Environment.NewLine;
                    }
                    else
                    {
                        tmpKidsBirthdays = txtFirstKidsBDay.Text;
                    }
                }
                kidsBirthdaysX = tmpKidsBirthdays.Split('/');
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Kid's Birthdays' Input..." + Environment.NewLine;
            }

            // Param #16 - Kids Education Status
            try
            {
                string tmpKidsEducationStatus = "-1";
                
                if (kidCountLevelX == 2 || kidCountLevelX == 3)
                {
                    if (rdbtnStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "1";
                    }
                    else if(rdbtnNotStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "0";
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Education Status' Choice..." + Environment.NewLine;
                    }

                    if (rdbtnStudentSecond.Checked)
                    {
                        tmpKidsEducationStatus += "/" + "1";
                    }
                    else if(rdbtnNotStudentSecond.Checked)
                    {
                        tmpKidsEducationStatus += "/" + "0";
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Second Kid's Education Status' Choice..." + Environment.NewLine;
                    }
                }
                else if (kidCountLevelX == 1)
                {
                    if (rdbtnStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "1";
                    }
                    else if(rdbtnNotStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "0";
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Education Status' Choice..." + Environment.NewLine;
                    }
                }
                kidsEducationStatusX = tmpKidsEducationStatus.Split('/').Select(Int32.Parse).ToArray();
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Kid's Education Status' Choice..." + Environment.NewLine;
            }

            // Param #17 - Salary
            try
            {
                salaryX = Double.Parse(txtSalary.Text); 
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Salary' Input..." + Environment.NewLine;
            }

            // Param #18 - Work Type
            try
            {
                if (rdbtnFull.Checked)
                {
                    fullTimeX = true;
                }
                else if(rdbtnPart.Checked)
                {
                    fullTimeX = false;
                }
                else
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Work Type' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Work Type' Choice..." + Environment.NewLine;
            }

            if(errorCheck==-1)
            {
                MessageBox.Show(errorMessage);
            }
            else
            {
                // Add Staff To Listview
                string staff = idX.ToString() + "," + nameX + "," + surnameX + "," + salaryX.ToString();
                mainForm.addStaffToList(staff);

                // Databese Staff Ekleme.
                staffListControl.addStaff(nameX, surnameX, homeCityX, workCityX, educationLevelX, foreignLanguageKnowledgeLevelX, knownOtherLanguagesX
                                        , jobLevelX, jobNotListedX, hiringDateX, experienceAtHiringDateX, maritalStatusX, partnersJobStatusX, kidCountLevelX
                                        , kidsBirthdaysX, kidsEducationStatusX, salaryX, fullTimeX);

                newStaffUI(); // Clear UI.
                MessageBox.Show("Staff Added Successfully.");
                this.Close();
            }
        }
        // Add Staff - End

        // Update Staff - Start
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            int errorCheck = 0;
            string errorMessage = "";

            string nameX = "";
            string surnameX = " ";
            string jobNotListedX = "-";
            string[] knownOtherLanguagesX = { "-" };
            string[] kidsBirthdaysX = { "-" };
            int homeCityX = -1;
            int workCityX = -1;
            int educationLevelX = -1;
            int foreignLanguageKnowledgeLevelX = -1;
            int jobLevelX = -1;
            int experienceAtHiringDateX = -1;
            int partnersJobStatusX = -1;
            int kidCountLevelX = -1;
            int[] kidsEducationStatusX = { -1 };
            double salaryX = 0;
            bool maritalStatusX = false;
            bool fullTimeX = false;

            int indexX = staffIndex; // Param #0 - Index to Update
            string hiringDateX = DateTime.UtcNow.ToString("yyyy"); // Param #10 - hiringDate
            int idX = staffListControl.staffIDofSelectedIndex(indexX); // ID of Selected Staff

            // Param #1 - Name
            try
            {
                nameX = txtName.Text;

                if (nameX.Length < 1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Name' Input..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Name' Input..." + Environment.NewLine;
            }

            // Param #2 - Surname
            try
            {
                surnameX = txtSurname.Text;

                if (surnameX.Length < 1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Surname' Input..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Surname' Input..." + Environment.NewLine;
            }

            // Param #3 - Home City
            try
            {
                homeCityX = cmboxHomeCity.SelectedIndex;

                if (homeCityX == -1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Home City' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Home City' Choice..." + Environment.NewLine;
            }

            // Param #4 - Work City
            try
            {
                workCityX = cmboxWorkCity.SelectedIndex;

                if (workCityX == -1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Work City' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Work City' Choice..." + Environment.NewLine;
            }

            // Param #5 - Education Level
            try
            {
                educationLevelX = cmboxEducation.SelectedIndex;

                if (educationLevelX == -1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Education' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Education' Choice..." + Environment.NewLine;
            }

            // Param #6 - Foreign Language Knowledge Level
            try
            {
                foreignLanguageKnowledgeLevelX = cmboxForeingLK.SelectedIndex;

                if (foreignLanguageKnowledgeLevelX == -1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Foreign Language Knowledge' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Foreign Language Knowledge' Choice..." + Environment.NewLine;
            }

            // Param #7 - Known Other Languages
            try
            {
                if (chckboxOtherLanguages.Checked)
                {
                    string tmpKnownOtherLanguages = txtOtherLanguages.Text;
                    if (tmpKnownOtherLanguages.Length > 0)
                    {
                        knownOtherLanguagesX = tmpKnownOtherLanguages.Split('/');
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Known Other Languages' Input..." + Environment.NewLine;
                    }
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Known Other Languages' Input..." + Environment.NewLine;
            }

            // Param #8 - Job Level
            try
            {
                jobLevelX = cmboxJob.SelectedIndex;

                if (jobLevelX == -1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Job' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Job' Choice..." + Environment.NewLine;
            }

            // Param #9 - Job Not Listed
            try
            {
                if (jobLevelX == 5)
                {
                    string tmpJobNotListed = txtJobNotListed.Text;

                    if (tmpJobNotListed.Length > 0)
                    {
                        jobNotListedX = tmpJobNotListed;
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Job Not Listed' Input..." + Environment.NewLine;
                    }
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Job Not Listed' Input..." + Environment.NewLine;
            }

            // Param #11 - Experience At Hiring Date
            try
            {
                experienceAtHiringDateX = Int32.Parse(txtExperience.Text);
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Experience' Input..." + Environment.NewLine;
            }

            // Param #12 - Marital Status
            try
            {
                if (rdbtnMarried.Checked)
                {
                    maritalStatusX = true;
                }
                else if (rdbtnSingle.Checked)
                {
                    maritalStatusX = false;
                }
                else
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Marital Status' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Marital Status' Choice..." + Environment.NewLine;
            }

            // Param #13 - Partners Job Status
            try
            {
                if (maritalStatusX)
                {
                    if (rdbtnWorking.Checked)
                    {
                        partnersJobStatusX = 1;
                    }
                    else if (rdbtnNotWorking.Checked)
                    {
                        partnersJobStatusX = 0;
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Partners Job Status' Choice..." + Environment.NewLine;
                    }
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Partners Job Status' Choice..." + Environment.NewLine;
            }

            // Param #14 - Kid Count Level
            try
            {
                kidCountLevelX = cmboxKidCount.SelectedIndex;

                if (kidCountLevelX == -1)
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Kid Count' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Kid Count' Choice..." + Environment.NewLine;
            }

            // Param #15 - Kids Birthdays
            try
            {
                string tmpKidsBirthdays = "-";

                if (kidCountLevelX == 2 || kidCountLevelX == 3)
                {
                    if (txtFirstKidsBDay.Text.Length < 10)
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Birthdays' Input..." + Environment.NewLine;
                    }
                    else
                    {
                        tmpKidsBirthdays = txtFirstKidsBDay.Text + "/";
                    }

                    if (txtSecondKidsBDay.Text.Length < 10)
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Second Kid's Birthdays' Input..." + Environment.NewLine;
                    }
                    else
                    {
                        tmpKidsBirthdays += txtSecondKidsBDay.Text;
                    }
                }
                else if (kidCountLevelX == 1)
                {
                    if (txtFirstKidsBDay.Text.Length < 10)
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Birthdays' Input..." + Environment.NewLine;
                    }
                    else
                    {
                        tmpKidsBirthdays = txtFirstKidsBDay.Text;
                    }
                }
                kidsBirthdaysX = tmpKidsBirthdays.Split('/');
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Kid's Birthdays' Input..." + Environment.NewLine;
            }

            // Param #16 - Kids Education Status
            try
            {
                string tmpKidsEducationStatus = "-1";

                if (kidCountLevelX == 2 || kidCountLevelX == 3)
                {
                    if (rdbtnStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "1";
                    }
                    else if (rdbtnNotStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "0";
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Education Status' Choice..." + Environment.NewLine;
                    }

                    if (rdbtnStudentSecond.Checked)
                    {
                        tmpKidsEducationStatus += "/" + "1";
                    }
                    else if (rdbtnNotStudentSecond.Checked)
                    {
                        tmpKidsEducationStatus += "/" + "0";
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'Second Kid's Education Status' Choice..." + Environment.NewLine;
                    }
                }
                else if (kidCountLevelX == 1)
                {
                    if (rdbtnStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "1";
                    }
                    else if (rdbtnNotStudentFirst.Checked)
                    {
                        tmpKidsEducationStatus = "0";
                    }
                    else
                    {
                        errorCheck = -1;
                        errorMessage += "Wrong 'First Kid's Education Status' Choice..." + Environment.NewLine;
                    }
                }
                kidsEducationStatusX = tmpKidsEducationStatus.Split('/').Select(Int32.Parse).ToArray();
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Kid's Education Status' Choice..." + Environment.NewLine;
            }

            // Param #17 - Salary
            try
            {
                salaryX = Double.Parse(txtSalary.Text);
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Salary' Input..." + Environment.NewLine;
            }

            // Param #18 - Work Type
            try
            {
                if (rdbtnFull.Checked)
                {
                    fullTimeX = true;
                }
                else if (rdbtnPart.Checked)
                {
                    fullTimeX = false;
                }
                else
                {
                    errorCheck = -1;
                    errorMessage += "Wrong 'Work Type' Choice..." + Environment.NewLine;
                }
            }
            catch
            {
                errorCheck = -1;
                errorMessage += "Wrong 'Work Type' Choice..." + Environment.NewLine;
            }

            if (errorCheck == -1)
            {
                MessageBox.Show(errorMessage);
            }
            else
            {
                // Add Staff To Listview
                string staff = idX.ToString() + "," + nameX + "," + surnameX + "," + salaryX.ToString();
                mainForm.updateStaffFromList(idX, staff);

                // Database Staff Update
                staffListControl.updateStaff(indexX, nameX, surnameX, homeCityX, workCityX, educationLevelX, foreignLanguageKnowledgeLevelX, knownOtherLanguagesX
                                        , jobLevelX, jobNotListedX, hiringDateX, experienceAtHiringDateX, maritalStatusX, partnersJobStatusX, kidCountLevelX
                                        , kidsBirthdaysX, kidsEducationStatusX, salaryX, fullTimeX);

                newStaffUI();
                MessageBox.Show("Staff Updated Successfully.");
                this.Close();
            }
        }
        // Update Staff - End

        // Delete Staff - Start
        private void btnDelete_Click(object sender, EventArgs e)
        {
            int indexX = staffIndex;
            int idX = staffListControl.staffIDofSelectedIndex(indexX);
            mainForm.deleteStaffFromList(idX);
            staffListControl.deleteStaff(indexX);
            MessageBox.Show("Staff Deleted Successfully.");
            this.Close();
        }
        // Delete Staff - End

        // Upload Photo to Staff - Start
        private void btnUploadPhoto_Click(object sender, EventArgs e)
        {
            openFileUploadPhoto.Filter = "JPG Dosyası|*.jpg";
            string path = @"C:\Users\Kartal\Desktop\Test\";
            string databaseFileName = "";

            if (StaffIndex > -1)
            {
                databaseFileName = (staffListControl.staffIDofSelectedIndex(StaffIndex)).ToString();
            }
            else
            {
                databaseFileName = (staffListControl.StaffID).ToString();
            }

            if (openFileUploadPhoto.ShowDialog() == DialogResult.OK)
            {
                File.Copy(openFileUploadPhoto.FileName, Path.Combine(path, Path.GetFileName(databaseFileName + ".jpg")), true);
                picboxStaffAvatar.Image = new Bitmap(openFileUploadPhoto.OpenFile());
            }

            MessageBox.Show("Photo Uploaded Successfully For User With ID: " + databaseFileName);
        }
        // Upload Photo to Staff - End
    }
}