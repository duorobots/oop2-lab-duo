﻿namespace Staff_Management1
{
    partial class StaffManagementForm
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblExperience = new System.Windows.Forms.Label();
            this.lblHomeCity = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.lblJob = new System.Windows.Forms.Label();
            this.txtExperience = new System.Windows.Forms.TextBox();
            this.txtJobNotListed = new System.Windows.Forms.TextBox();
            this.lblWorkCity = new System.Windows.Forms.Label();
            this.lblEducation = new System.Windows.Forms.Label();
            this.cmboxHomeCity = new System.Windows.Forms.ComboBox();
            this.cmboxWorkCity = new System.Windows.Forms.ComboBox();
            this.cmboxEducation = new System.Windows.Forms.ComboBox();
            this.cmboxJob = new System.Windows.Forms.ComboBox();
            this.lblForeignLK1 = new System.Windows.Forms.Label();
            this.lblForeignLK2 = new System.Windows.Forms.Label();
            this.lblJobNotListed = new System.Windows.Forms.Label();
            this.lblMaritalStatus = new System.Windows.Forms.Label();
            this.rdbtnMarried = new System.Windows.Forms.RadioButton();
            this.rdbtnSingle = new System.Windows.Forms.RadioButton();
            this.lblPartnersJobStatus = new System.Windows.Forms.Label();
            this.rdbtnWorking = new System.Windows.Forms.RadioButton();
            this.rdbtnNotWorking = new System.Windows.Forms.RadioButton();
            this.lblKidCount = new System.Windows.Forms.Label();
            this.cmboxKidCount = new System.Windows.Forms.ComboBox();
            this.txtFirstKidsBDay = new System.Windows.Forms.TextBox();
            this.lblFirstKidsBDay = new System.Windows.Forms.Label();
            this.rdbtnNotStudentFirst = new System.Windows.Forms.RadioButton();
            this.rdbtnStudentFirst = new System.Windows.Forms.RadioButton();
            this.lblFirstKidsEducation = new System.Windows.Forms.Label();
            this.chckboxOtherLanguages = new System.Windows.Forms.CheckBox();
            this.cmboxForeingLK = new System.Windows.Forms.ComboBox();
            this.txtOtherLanguages = new System.Windows.Forms.TextBox();
            this.lblOtherLanguages1 = new System.Windows.Forms.Label();
            this.lblSecondKidsEducation = new System.Windows.Forms.Label();
            this.lblSecondKidsBDay = new System.Windows.Forms.Label();
            this.txtSecondKidsBDay = new System.Windows.Forms.TextBox();
            this.grpboxMarital = new System.Windows.Forms.GroupBox();
            this.grpboxKidsEducationFirst = new System.Windows.Forms.GroupBox();
            this.grpboxPartnersJob = new System.Windows.Forms.GroupBox();
            this.grpboxKidsEducationSecond = new System.Windows.Forms.GroupBox();
            this.rdbtnStudentSecond = new System.Windows.Forms.RadioButton();
            this.rdbtnNotStudentSecond = new System.Windows.Forms.RadioButton();
            this.lblOtherLanguages2 = new System.Windows.Forms.Label();
            this.openFileUploadPhoto = new System.Windows.Forms.OpenFileDialog();
            this.picboxStaffAvatar = new System.Windows.Forms.PictureBox();
            this.btnUploadPhoto = new System.Windows.Forms.Button();
            this.lblWorkType = new System.Windows.Forms.Label();
            this.rdbtnFull = new System.Windows.Forms.RadioButton();
            this.rdbtnPart = new System.Windows.Forms.RadioButton();
            this.grpbxWorkType = new System.Windows.Forms.GroupBox();
            this.txtSalary = new System.Windows.Forms.MaskedTextBox();
            this.lblBMOSalary = new System.Windows.Forms.Label();
            this.grpboxMarital.SuspendLayout();
            this.grpboxKidsEducationFirst.SuspendLayout();
            this.grpboxPartnersJob.SuspendLayout();
            this.grpboxKidsEducationSecond.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxStaffAvatar)).BeginInit();
            this.grpbxWorkType.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(139, 32);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(310, 20);
            this.txtName.TabIndex = 0;
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(139, 58);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(310, 20);
            this.txtSurname.TabIndex = 1;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(15, 572);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(65, 23);
            this.btnAdd.TabIndex = 23;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(207, 572);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(65, 23);
            this.btnUpdate.TabIndex = 24;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(384, 572);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(65, 23);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 35);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name: ";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(12, 61);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(55, 13);
            this.lblSurname.TabIndex = 0;
            this.lblSurname.Text = "Surname: ";
            // 
            // lblExperience
            // 
            this.lblExperience.AutoSize = true;
            this.lblExperience.Location = new System.Drawing.Point(12, 303);
            this.lblExperience.Name = "lblExperience";
            this.lblExperience.Size = new System.Drawing.Size(99, 13);
            this.lblExperience.TabIndex = 0;
            this.lblExperience.Text = "Experience (Years):";
            // 
            // lblHomeCity
            // 
            this.lblHomeCity.AutoSize = true;
            this.lblHomeCity.Location = new System.Drawing.Point(12, 87);
            this.lblHomeCity.Name = "lblHomeCity";
            this.lblHomeCity.Size = new System.Drawing.Size(61, 13);
            this.lblHomeCity.TabIndex = 0;
            this.lblHomeCity.Text = "Home City: ";
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(12, 498);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(42, 13);
            this.lblSalary.TabIndex = 0;
            this.lblSalary.Text = "Salary: ";
            // 
            // lblJob
            // 
            this.lblJob.AutoSize = true;
            this.lblJob.Location = new System.Drawing.Point(12, 250);
            this.lblJob.Name = "lblJob";
            this.lblJob.Size = new System.Drawing.Size(30, 13);
            this.lblJob.TabIndex = 0;
            this.lblJob.Text = "Job: ";
            // 
            // txtExperience
            // 
            this.txtExperience.Location = new System.Drawing.Point(139, 300);
            this.txtExperience.Name = "txtExperience";
            this.txtExperience.Size = new System.Drawing.Size(310, 20);
            this.txtExperience.TabIndex = 10;
            // 
            // txtJobNotListed
            // 
            this.txtJobNotListed.Location = new System.Drawing.Point(139, 274);
            this.txtJobNotListed.Name = "txtJobNotListed";
            this.txtJobNotListed.Size = new System.Drawing.Size(310, 20);
            this.txtJobNotListed.TabIndex = 9;
            // 
            // lblWorkCity
            // 
            this.lblWorkCity.AutoSize = true;
            this.lblWorkCity.Location = new System.Drawing.Point(12, 114);
            this.lblWorkCity.Name = "lblWorkCity";
            this.lblWorkCity.Size = new System.Drawing.Size(59, 13);
            this.lblWorkCity.TabIndex = 0;
            this.lblWorkCity.Text = "Work City: ";
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Location = new System.Drawing.Point(12, 141);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(58, 13);
            this.lblEducation.TabIndex = 0;
            this.lblEducation.Text = "Education:";
            // 
            // cmboxHomeCity
            // 
            this.cmboxHomeCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxHomeCity.FormattingEnabled = true;
            this.cmboxHomeCity.Items.AddRange(new object[] {
            "Adana",
            "Adıyaman",
            "Afyonkarahisar",
            "Ağrı",
            "Aksaray",
            "Amasya",
            "Ankara",
            "Antalya",
            "Ardahan",
            "Artvin",
            "Aydın",
            "Balıkesir",
            "Bartın",
            "Batman",
            "Bayburt",
            "Bilecik",
            "Bingöl",
            "Bitlis",
            "Bolu",
            "Burdur",
            "Bursa",
            "Çanakkale",
            "Çankırı",
            "Çorum",
            "Denizli",
            "Diyarbakır",
            "Düzce",
            "Edirne",
            "Elazığ",
            "Erzincan",
            "Erzurum",
            "Eskişehir",
            "Gaziantep",
            "Giresun",
            "Gümüşhane",
            "Hakkari",
            "Hatay",
            "Iğdır",
            "Isparta",
            "İstanbul",
            "İzmir",
            "Kahramanmaraş",
            "Karabük",
            "Karaman",
            "Kars",
            "Kastamonu",
            "Kayseri",
            "Kırıkkale",
            "Kırklareli",
            "Kırşehir",
            "Kilis",
            "Kocaeli",
            "Konya",
            "Kütahya",
            "Malatya",
            "Manisa",
            "Mardin",
            "Mersin",
            "Muğla",
            "Muş",
            "Nevşehir",
            "Niğde",
            "Ordu",
            "Osmaniye",
            "Rize",
            "Sakarya",
            "Samsun",
            "Siirt",
            "Sinop",
            "Sivas",
            "Şanlıurfa",
            "Şırnak",
            "Tekirdağ",
            "Tokat",
            "Trabzon",
            "Tunceli",
            "Uşak",
            "Van",
            "Yalova",
            "Yozgat",
            "Zonguldak"});
            this.cmboxHomeCity.Location = new System.Drawing.Point(139, 84);
            this.cmboxHomeCity.Name = "cmboxHomeCity";
            this.cmboxHomeCity.Size = new System.Drawing.Size(310, 21);
            this.cmboxHomeCity.TabIndex = 2;
            // 
            // cmboxWorkCity
            // 
            this.cmboxWorkCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxWorkCity.FormattingEnabled = true;
            this.cmboxWorkCity.Items.AddRange(new object[] {
            "Adana",
            "Adıyaman",
            "Afyonkarahisar",
            "Ağrı",
            "Aksaray",
            "Amasya",
            "Ankara",
            "Antalya",
            "Ardahan",
            "Artvin",
            "Aydın",
            "Balıkesir",
            "Bartın",
            "Batman",
            "Bayburt",
            "Bilecik",
            "Bingöl",
            "Bitlis",
            "Bolu",
            "Burdur",
            "Bursa",
            "Çanakkale",
            "Çankırı",
            "Çorum",
            "Denizli",
            "Diyarbakır",
            "Düzce",
            "Edirne",
            "Elazığ",
            "Erzincan",
            "Erzurum",
            "Eskişehir",
            "Gaziantep",
            "Giresun",
            "Gümüşhane",
            "Hakkari",
            "Hatay",
            "Iğdır",
            "Isparta",
            "İstanbul",
            "İzmir",
            "Kahramanmaraş",
            "Karabük",
            "Karaman",
            "Kars",
            "Kastamonu",
            "Kayseri",
            "Kırıkkale",
            "Kırklareli",
            "Kırşehir",
            "Kilis",
            "Kocaeli",
            "Konya",
            "Kütahya",
            "Malatya",
            "Manisa",
            "Mardin",
            "Mersin",
            "Muğla",
            "Muş",
            "Nevşehir",
            "Niğde",
            "Ordu",
            "Osmaniye",
            "Rize",
            "Sakarya",
            "Samsun",
            "Siirt",
            "Sinop",
            "Sivas",
            "Şanlıurfa",
            "Şırnak",
            "Tekirdağ",
            "Tokat",
            "Trabzon",
            "Tunceli",
            "Uşak",
            "Van",
            "Yalova",
            "Yozgat",
            "Zonguldak"});
            this.cmboxWorkCity.Location = new System.Drawing.Point(139, 111);
            this.cmboxWorkCity.Name = "cmboxWorkCity";
            this.cmboxWorkCity.Size = new System.Drawing.Size(310, 21);
            this.cmboxWorkCity.TabIndex = 3;
            // 
            // cmboxEducation
            // 
            this.cmboxEducation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxEducation.FormattingEnabled = true;
            this.cmboxEducation.Items.AddRange(new object[] {
            "Üniversite Eğitimi Yok",
            "Önlisans",
            "Lisans",
            "Mesleki Alanda Yüksek Lisans",
            "Mesleki Alanda Doktora",
            "Mesleki Alanda Doçentlik",
            "Mesleki Alan Harici Yüksek Lisans",
            "Mesleki Alan Harici Doktora / Doçentlik"});
            this.cmboxEducation.Location = new System.Drawing.Point(139, 138);
            this.cmboxEducation.Name = "cmboxEducation";
            this.cmboxEducation.Size = new System.Drawing.Size(310, 21);
            this.cmboxEducation.TabIndex = 4;
            // 
            // cmboxJob
            // 
            this.cmboxJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxJob.FormattingEnabled = true;
            this.cmboxJob.Items.AddRange(new object[] {
            "Takım Lideri / Grup Yöneticisi / Teknik Yönetici / Yazılım Mimarı",
            "Proje Yöneticisi",
            "Direktör / Projeler Yöneticisi",
            "CTO / Genel Müdür",
            "Bilgi İşlem Sorumlusu / Müdürü",
            "Diğer"});
            this.cmboxJob.Location = new System.Drawing.Point(139, 247);
            this.cmboxJob.Name = "cmboxJob";
            this.cmboxJob.Size = new System.Drawing.Size(310, 21);
            this.cmboxJob.TabIndex = 8;
            this.cmboxJob.SelectedIndexChanged += new System.EventHandler(this.cmboxJob_SelectedIndexChanged);
            // 
            // lblForeignLK1
            // 
            this.lblForeignLK1.AutoSize = true;
            this.lblForeignLK1.Location = new System.Drawing.Point(12, 165);
            this.lblForeignLK1.Name = "lblForeignLK1";
            this.lblForeignLK1.Size = new System.Drawing.Size(93, 13);
            this.lblForeignLK1.TabIndex = 0;
            this.lblForeignLK1.Text = "Foreign Language";
            // 
            // lblForeignLK2
            // 
            this.lblForeignLK2.AutoSize = true;
            this.lblForeignLK2.Location = new System.Drawing.Point(12, 178);
            this.lblForeignLK2.Name = "lblForeignLK2";
            this.lblForeignLK2.Size = new System.Drawing.Size(63, 13);
            this.lblForeignLK2.TabIndex = 0;
            this.lblForeignLK2.Text = "Knowledge:";
            // 
            // lblJobNotListed
            // 
            this.lblJobNotListed.AutoSize = true;
            this.lblJobNotListed.Location = new System.Drawing.Point(12, 277);
            this.lblJobNotListed.Name = "lblJobNotListed";
            this.lblJobNotListed.Size = new System.Drawing.Size(87, 13);
            this.lblJobNotListed.TabIndex = 0;
            this.lblJobNotListed.Text = "Job (Not Listed): ";
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.AutoSize = true;
            this.lblMaritalStatus.Location = new System.Drawing.Point(12, 328);
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.Size = new System.Drawing.Size(74, 13);
            this.lblMaritalStatus.TabIndex = 0;
            this.lblMaritalStatus.Text = "Marital Status:";
            // 
            // rdbtnMarried
            // 
            this.rdbtnMarried.AutoSize = true;
            this.rdbtnMarried.Location = new System.Drawing.Point(68, 0);
            this.rdbtnMarried.Name = "rdbtnMarried";
            this.rdbtnMarried.Size = new System.Drawing.Size(60, 17);
            this.rdbtnMarried.TabIndex = 12;
            this.rdbtnMarried.TabStop = true;
            this.rdbtnMarried.Text = "Married";
            this.rdbtnMarried.UseVisualStyleBackColor = true;
            this.rdbtnMarried.Enter += new System.EventHandler(this.rdbtnMarried_Enter);
            // 
            // rdbtnSingle
            // 
            this.rdbtnSingle.AutoSize = true;
            this.rdbtnSingle.Location = new System.Drawing.Point(0, 0);
            this.rdbtnSingle.Name = "rdbtnSingle";
            this.rdbtnSingle.Size = new System.Drawing.Size(54, 17);
            this.rdbtnSingle.TabIndex = 11;
            this.rdbtnSingle.TabStop = true;
            this.rdbtnSingle.Text = "Single";
            this.rdbtnSingle.UseVisualStyleBackColor = true;
            this.rdbtnSingle.Enter += new System.EventHandler(this.rdbtnSingle_Enter);
            // 
            // lblPartnersJobStatus
            // 
            this.lblPartnersJobStatus.AutoSize = true;
            this.lblPartnersJobStatus.Location = new System.Drawing.Point(12, 349);
            this.lblPartnersJobStatus.Name = "lblPartnersJobStatus";
            this.lblPartnersJobStatus.Size = new System.Drawing.Size(104, 13);
            this.lblPartnersJobStatus.TabIndex = 0;
            this.lblPartnersJobStatus.Text = "Partner\'s Job Status:";
            // 
            // rdbtnWorking
            // 
            this.rdbtnWorking.AutoSize = true;
            this.rdbtnWorking.Location = new System.Drawing.Point(0, 3);
            this.rdbtnWorking.Name = "rdbtnWorking";
            this.rdbtnWorking.Size = new System.Drawing.Size(65, 17);
            this.rdbtnWorking.TabIndex = 13;
            this.rdbtnWorking.TabStop = true;
            this.rdbtnWorking.Text = "Working";
            this.rdbtnWorking.UseVisualStyleBackColor = true;
            // 
            // rdbtnNotWorking
            // 
            this.rdbtnNotWorking.AutoSize = true;
            this.rdbtnNotWorking.Location = new System.Drawing.Point(68, 3);
            this.rdbtnNotWorking.Name = "rdbtnNotWorking";
            this.rdbtnNotWorking.Size = new System.Drawing.Size(85, 17);
            this.rdbtnNotWorking.TabIndex = 14;
            this.rdbtnNotWorking.TabStop = true;
            this.rdbtnNotWorking.Text = "Not Working";
            this.rdbtnNotWorking.UseVisualStyleBackColor = true;
            // 
            // lblKidCount
            // 
            this.lblKidCount.AutoSize = true;
            this.lblKidCount.Location = new System.Drawing.Point(12, 373);
            this.lblKidCount.Name = "lblKidCount";
            this.lblKidCount.Size = new System.Drawing.Size(56, 13);
            this.lblKidCount.TabIndex = 0;
            this.lblKidCount.Text = "Kid Count:";
            // 
            // cmboxKidCount
            // 
            this.cmboxKidCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxKidCount.FormattingEnabled = true;
            this.cmboxKidCount.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "2+"});
            this.cmboxKidCount.Location = new System.Drawing.Point(139, 370);
            this.cmboxKidCount.Name = "cmboxKidCount";
            this.cmboxKidCount.Size = new System.Drawing.Size(310, 21);
            this.cmboxKidCount.TabIndex = 15;
            this.cmboxKidCount.SelectedIndexChanged += new System.EventHandler(this.cmboxKidCount_SelectedIndexChanged);
            // 
            // txtFirstKidsBDay
            // 
            this.txtFirstKidsBDay.Location = new System.Drawing.Point(139, 397);
            this.txtFirstKidsBDay.Name = "txtFirstKidsBDay";
            this.txtFirstKidsBDay.Size = new System.Drawing.Size(310, 20);
            this.txtFirstKidsBDay.TabIndex = 16;
            // 
            // lblFirstKidsBDay
            // 
            this.lblFirstKidsBDay.AutoSize = true;
            this.lblFirstKidsBDay.Location = new System.Drawing.Point(11, 400);
            this.lblFirstKidsBDay.Name = "lblFirstKidsBDay";
            this.lblFirstKidsBDay.Size = new System.Drawing.Size(95, 13);
            this.lblFirstKidsBDay.TabIndex = 0;
            this.lblFirstKidsBDay.Text = "First Kid\'s Birthday:";
            // 
            // rdbtnNotStudentFirst
            // 
            this.rdbtnNotStudentFirst.AutoSize = true;
            this.rdbtnNotStudentFirst.Location = new System.Drawing.Point(68, 0);
            this.rdbtnNotStudentFirst.Name = "rdbtnNotStudentFirst";
            this.rdbtnNotStudentFirst.Size = new System.Drawing.Size(82, 17);
            this.rdbtnNotStudentFirst.TabIndex = 18;
            this.rdbtnNotStudentFirst.TabStop = true;
            this.rdbtnNotStudentFirst.Text = "Not Student";
            this.rdbtnNotStudentFirst.UseVisualStyleBackColor = true;
            // 
            // rdbtnStudentFirst
            // 
            this.rdbtnStudentFirst.AutoSize = true;
            this.rdbtnStudentFirst.Location = new System.Drawing.Point(0, 0);
            this.rdbtnStudentFirst.Name = "rdbtnStudentFirst";
            this.rdbtnStudentFirst.Size = new System.Drawing.Size(62, 17);
            this.rdbtnStudentFirst.TabIndex = 17;
            this.rdbtnStudentFirst.TabStop = true;
            this.rdbtnStudentFirst.Text = "Student";
            this.rdbtnStudentFirst.UseVisualStyleBackColor = true;
            // 
            // lblFirstKidsEducation
            // 
            this.lblFirstKidsEducation.AutoSize = true;
            this.lblFirstKidsEducation.Location = new System.Drawing.Point(12, 425);
            this.lblFirstKidsEducation.Name = "lblFirstKidsEducation";
            this.lblFirstKidsEducation.Size = new System.Drawing.Size(105, 13);
            this.lblFirstKidsEducation.TabIndex = 0;
            this.lblFirstKidsEducation.Text = "First Kid\'s Education:";
            // 
            // chckboxOtherLanguages
            // 
            this.chckboxOtherLanguages.AutoSize = true;
            this.chckboxOtherLanguages.Location = new System.Drawing.Point(139, 192);
            this.chckboxOtherLanguages.Name = "chckboxOtherLanguages";
            this.chckboxOtherLanguages.Size = new System.Drawing.Size(108, 17);
            this.chckboxOtherLanguages.TabIndex = 6;
            this.chckboxOtherLanguages.Text = "Other Languages";
            this.chckboxOtherLanguages.UseVisualStyleBackColor = true;
            this.chckboxOtherLanguages.CheckedChanged += new System.EventHandler(this.chckboxOtherLanguages_CheckedChanged);
            // 
            // cmboxForeingLK
            // 
            this.cmboxForeingLK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxForeingLK.FormattingEnabled = true;
            this.cmboxForeingLK.Items.AddRange(new object[] {
            "İngilizce Dil Bilgisi Yok",
            "Belgelendirilmiş İngilizce Bilgisi",
            "İngilizce Eğitim Veren Okul Mezuniyeti"});
            this.cmboxForeingLK.Location = new System.Drawing.Point(139, 165);
            this.cmboxForeingLK.Name = "cmboxForeingLK";
            this.cmboxForeingLK.Size = new System.Drawing.Size(310, 21);
            this.cmboxForeingLK.TabIndex = 5;
            // 
            // txtOtherLanguages
            // 
            this.txtOtherLanguages.Location = new System.Drawing.Point(139, 215);
            this.txtOtherLanguages.Name = "txtOtherLanguages";
            this.txtOtherLanguages.Size = new System.Drawing.Size(310, 20);
            this.txtOtherLanguages.TabIndex = 7;
            // 
            // lblOtherLanguages1
            // 
            this.lblOtherLanguages1.AutoSize = true;
            this.lblOtherLanguages1.Location = new System.Drawing.Point(12, 215);
            this.lblOtherLanguages1.Name = "lblOtherLanguages1";
            this.lblOtherLanguages1.Size = new System.Drawing.Size(92, 13);
            this.lblOtherLanguages1.TabIndex = 0;
            this.lblOtherLanguages1.Text = "Other Languages:";
            // 
            // lblSecondKidsEducation
            // 
            this.lblSecondKidsEducation.AutoSize = true;
            this.lblSecondKidsEducation.Location = new System.Drawing.Point(12, 474);
            this.lblSecondKidsEducation.Name = "lblSecondKidsEducation";
            this.lblSecondKidsEducation.Size = new System.Drawing.Size(123, 13);
            this.lblSecondKidsEducation.TabIndex = 0;
            this.lblSecondKidsEducation.Text = "Second Kid\'s Education:";
            // 
            // lblSecondKidsBDay
            // 
            this.lblSecondKidsBDay.AutoSize = true;
            this.lblSecondKidsBDay.Location = new System.Drawing.Point(12, 449);
            this.lblSecondKidsBDay.Name = "lblSecondKidsBDay";
            this.lblSecondKidsBDay.Size = new System.Drawing.Size(113, 13);
            this.lblSecondKidsBDay.TabIndex = 0;
            this.lblSecondKidsBDay.Text = "Second Kid\'s Birthday:";
            // 
            // txtSecondKidsBDay
            // 
            this.txtSecondKidsBDay.Location = new System.Drawing.Point(139, 446);
            this.txtSecondKidsBDay.Name = "txtSecondKidsBDay";
            this.txtSecondKidsBDay.Size = new System.Drawing.Size(310, 20);
            this.txtSecondKidsBDay.TabIndex = 19;
            // 
            // grpboxMarital
            // 
            this.grpboxMarital.Controls.Add(this.rdbtnSingle);
            this.grpboxMarital.Controls.Add(this.rdbtnMarried);
            this.grpboxMarital.Location = new System.Drawing.Point(139, 326);
            this.grpboxMarital.Name = "grpboxMarital";
            this.grpboxMarital.Size = new System.Drawing.Size(150, 20);
            this.grpboxMarital.TabIndex = 52;
            this.grpboxMarital.TabStop = false;
            // 
            // grpboxKidsEducationFirst
            // 
            this.grpboxKidsEducationFirst.Controls.Add(this.rdbtnStudentFirst);
            this.grpboxKidsEducationFirst.Controls.Add(this.rdbtnNotStudentFirst);
            this.grpboxKidsEducationFirst.Location = new System.Drawing.Point(139, 423);
            this.grpboxKidsEducationFirst.Name = "grpboxKidsEducationFirst";
            this.grpboxKidsEducationFirst.Size = new System.Drawing.Size(150, 20);
            this.grpboxKidsEducationFirst.TabIndex = 53;
            this.grpboxKidsEducationFirst.TabStop = false;
            // 
            // grpboxPartnersJob
            // 
            this.grpboxPartnersJob.Controls.Add(this.rdbtnWorking);
            this.grpboxPartnersJob.Controls.Add(this.rdbtnNotWorking);
            this.grpboxPartnersJob.Location = new System.Drawing.Point(139, 344);
            this.grpboxPartnersJob.Name = "grpboxPartnersJob";
            this.grpboxPartnersJob.Size = new System.Drawing.Size(150, 20);
            this.grpboxPartnersJob.TabIndex = 54;
            this.grpboxPartnersJob.TabStop = false;
            // 
            // grpboxKidsEducationSecond
            // 
            this.grpboxKidsEducationSecond.Controls.Add(this.rdbtnStudentSecond);
            this.grpboxKidsEducationSecond.Controls.Add(this.rdbtnNotStudentSecond);
            this.grpboxKidsEducationSecond.Location = new System.Drawing.Point(139, 472);
            this.grpboxKidsEducationSecond.Name = "grpboxKidsEducationSecond";
            this.grpboxKidsEducationSecond.Size = new System.Drawing.Size(150, 20);
            this.grpboxKidsEducationSecond.TabIndex = 55;
            this.grpboxKidsEducationSecond.TabStop = false;
            // 
            // rdbtnStudentSecond
            // 
            this.rdbtnStudentSecond.AutoSize = true;
            this.rdbtnStudentSecond.Location = new System.Drawing.Point(0, 0);
            this.rdbtnStudentSecond.Name = "rdbtnStudentSecond";
            this.rdbtnStudentSecond.Size = new System.Drawing.Size(62, 17);
            this.rdbtnStudentSecond.TabIndex = 20;
            this.rdbtnStudentSecond.TabStop = true;
            this.rdbtnStudentSecond.Text = "Student";
            this.rdbtnStudentSecond.UseVisualStyleBackColor = true;
            // 
            // rdbtnNotStudentSecond
            // 
            this.rdbtnNotStudentSecond.AutoSize = true;
            this.rdbtnNotStudentSecond.Location = new System.Drawing.Point(68, 0);
            this.rdbtnNotStudentSecond.Name = "rdbtnNotStudentSecond";
            this.rdbtnNotStudentSecond.Size = new System.Drawing.Size(82, 17);
            this.rdbtnNotStudentSecond.TabIndex = 21;
            this.rdbtnNotStudentSecond.TabStop = true;
            this.rdbtnNotStudentSecond.Text = "Not Student";
            this.rdbtnNotStudentSecond.UseVisualStyleBackColor = true;
            // 
            // lblOtherLanguages2
            // 
            this.lblOtherLanguages2.AutoSize = true;
            this.lblOtherLanguages2.Location = new System.Drawing.Point(12, 228);
            this.lblOtherLanguages2.Name = "lblOtherLanguages2";
            this.lblOtherLanguages2.Size = new System.Drawing.Size(89, 13);
            this.lblOtherLanguages2.TabIndex = 0;
            this.lblOtherLanguages2.Text = "(Separate with / )";
            // 
            // picboxStaffAvatar
            // 
            this.picboxStaffAvatar.Location = new System.Drawing.Point(562, 32);
            this.picboxStaffAvatar.Name = "picboxStaffAvatar";
            this.picboxStaffAvatar.Size = new System.Drawing.Size(200, 200);
            this.picboxStaffAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picboxStaffAvatar.TabIndex = 56;
            this.picboxStaffAvatar.TabStop = false;
            // 
            // btnUploadPhoto
            // 
            this.btnUploadPhoto.Location = new System.Drawing.Point(587, 238);
            this.btnUploadPhoto.Name = "btnUploadPhoto";
            this.btnUploadPhoto.Size = new System.Drawing.Size(150, 50);
            this.btnUploadPhoto.TabIndex = 57;
            this.btnUploadPhoto.Text = "Upload Photo";
            this.btnUploadPhoto.UseVisualStyleBackColor = true;
            this.btnUploadPhoto.Click += new System.EventHandler(this.btnUploadPhoto_Click);
            // 
            // lblWorkType
            // 
            this.lblWorkType.AutoSize = true;
            this.lblWorkType.Location = new System.Drawing.Point(12, 532);
            this.lblWorkType.Name = "lblWorkType";
            this.lblWorkType.Size = new System.Drawing.Size(66, 13);
            this.lblWorkType.TabIndex = 58;
            this.lblWorkType.Text = "Work Type: ";
            // 
            // rdbtnFull
            // 
            this.rdbtnFull.AutoSize = true;
            this.rdbtnFull.Location = new System.Drawing.Point(0, 9);
            this.rdbtnFull.Name = "rdbtnFull";
            this.rdbtnFull.Size = new System.Drawing.Size(67, 17);
            this.rdbtnFull.TabIndex = 59;
            this.rdbtnFull.TabStop = true;
            this.rdbtnFull.Text = "Full-Time";
            this.rdbtnFull.UseVisualStyleBackColor = true;
            // 
            // rdbtnPart
            // 
            this.rdbtnPart.AutoSize = true;
            this.rdbtnPart.Location = new System.Drawing.Point(73, 9);
            this.rdbtnPart.Name = "rdbtnPart";
            this.rdbtnPart.Size = new System.Drawing.Size(70, 17);
            this.rdbtnPart.TabIndex = 60;
            this.rdbtnPart.TabStop = true;
            this.rdbtnPart.Text = "Part-Time";
            this.rdbtnPart.UseVisualStyleBackColor = true;
            // 
            // grpbxWorkType
            // 
            this.grpbxWorkType.Controls.Add(this.rdbtnFull);
            this.grpbxWorkType.Controls.Add(this.rdbtnPart);
            this.grpbxWorkType.Location = new System.Drawing.Point(139, 521);
            this.grpbxWorkType.Name = "grpbxWorkType";
            this.grpbxWorkType.Size = new System.Drawing.Size(160, 24);
            this.grpbxWorkType.TabIndex = 62;
            this.grpbxWorkType.TabStop = false;
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(139, 498);
            this.txtSalary.Mask = "0000000";
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(310, 20);
            this.txtSalary.TabIndex = 63;
            // 
            // lblBMOSalary
            // 
            this.lblBMOSalary.AutoSize = true;
            this.lblBMOSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblBMOSalary.Location = new System.Drawing.Point(558, 498);
            this.lblBMOSalary.Name = "lblBMOSalary";
            this.lblBMOSalary.Size = new System.Drawing.Size(101, 20);
            this.lblBMOSalary.TabIndex = 64;
            this.lblBMOSalary.Text = "BMO Salary: ";
            // 
            // StaffManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 607);
            this.Controls.Add(this.lblBMOSalary);
            this.Controls.Add(this.txtSalary);
            this.Controls.Add(this.grpbxWorkType);
            this.Controls.Add(this.lblWorkType);
            this.Controls.Add(this.btnUploadPhoto);
            this.Controls.Add(this.picboxStaffAvatar);
            this.Controls.Add(this.lblOtherLanguages2);
            this.Controls.Add(this.grpboxKidsEducationSecond);
            this.Controls.Add(this.grpboxPartnersJob);
            this.Controls.Add(this.grpboxKidsEducationFirst);
            this.Controls.Add(this.grpboxMarital);
            this.Controls.Add(this.lblSecondKidsBDay);
            this.Controls.Add(this.txtSecondKidsBDay);
            this.Controls.Add(this.lblSecondKidsEducation);
            this.Controls.Add(this.lblOtherLanguages1);
            this.Controls.Add(this.txtOtherLanguages);
            this.Controls.Add(this.cmboxForeingLK);
            this.Controls.Add(this.chckboxOtherLanguages);
            this.Controls.Add(this.lblFirstKidsEducation);
            this.Controls.Add(this.lblFirstKidsBDay);
            this.Controls.Add(this.txtFirstKidsBDay);
            this.Controls.Add(this.cmboxKidCount);
            this.Controls.Add(this.lblKidCount);
            this.Controls.Add(this.lblPartnersJobStatus);
            this.Controls.Add(this.lblMaritalStatus);
            this.Controls.Add(this.lblJobNotListed);
            this.Controls.Add(this.lblForeignLK2);
            this.Controls.Add(this.lblForeignLK1);
            this.Controls.Add(this.cmboxJob);
            this.Controls.Add(this.cmboxEducation);
            this.Controls.Add(this.cmboxWorkCity);
            this.Controls.Add(this.cmboxHomeCity);
            this.Controls.Add(this.lblEducation);
            this.Controls.Add(this.lblWorkCity);
            this.Controls.Add(this.txtJobNotListed);
            this.Controls.Add(this.txtExperience);
            this.Controls.Add(this.lblJob);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.lblHomeCity);
            this.Controls.Add(this.lblExperience);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtSurname);
            this.Controls.Add(this.txtName);
            this.Name = "StaffManagementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff Management";
            this.Load += new System.EventHandler(this.StaffManagementForm_Load);
            this.grpboxMarital.ResumeLayout(false);
            this.grpboxMarital.PerformLayout();
            this.grpboxKidsEducationFirst.ResumeLayout(false);
            this.grpboxKidsEducationFirst.PerformLayout();
            this.grpboxPartnersJob.ResumeLayout(false);
            this.grpboxPartnersJob.PerformLayout();
            this.grpboxKidsEducationSecond.ResumeLayout(false);
            this.grpboxKidsEducationSecond.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxStaffAvatar)).EndInit();
            this.grpbxWorkType.ResumeLayout(false);
            this.grpbxWorkType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblExperience;
        private System.Windows.Forms.Label lblHomeCity;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.Label lblJob;
        private System.Windows.Forms.TextBox txtExperience;
        private System.Windows.Forms.TextBox txtJobNotListed;
        private System.Windows.Forms.Label lblWorkCity;
        private System.Windows.Forms.Label lblEducation;
        private System.Windows.Forms.ComboBox cmboxHomeCity;
        private System.Windows.Forms.ComboBox cmboxWorkCity;
        private System.Windows.Forms.ComboBox cmboxEducation;
        private System.Windows.Forms.ComboBox cmboxJob;
        private System.Windows.Forms.Label lblForeignLK1;
        private System.Windows.Forms.Label lblForeignLK2;
        private System.Windows.Forms.Label lblJobNotListed;
        private System.Windows.Forms.Label lblMaritalStatus;
        private System.Windows.Forms.RadioButton rdbtnMarried;
        private System.Windows.Forms.RadioButton rdbtnSingle;
        private System.Windows.Forms.Label lblPartnersJobStatus;
        private System.Windows.Forms.RadioButton rdbtnWorking;
        private System.Windows.Forms.RadioButton rdbtnNotWorking;
        private System.Windows.Forms.Label lblKidCount;
        private System.Windows.Forms.ComboBox cmboxKidCount;
        private System.Windows.Forms.TextBox txtFirstKidsBDay;
        private System.Windows.Forms.Label lblFirstKidsBDay;
        private System.Windows.Forms.RadioButton rdbtnNotStudentFirst;
        private System.Windows.Forms.RadioButton rdbtnStudentFirst;
        private System.Windows.Forms.Label lblFirstKidsEducation;
        private System.Windows.Forms.CheckBox chckboxOtherLanguages;
        private System.Windows.Forms.ComboBox cmboxForeingLK;
        private System.Windows.Forms.TextBox txtOtherLanguages;
        private System.Windows.Forms.Label lblOtherLanguages1;
        private System.Windows.Forms.Label lblSecondKidsEducation;
        private System.Windows.Forms.Label lblSecondKidsBDay;
        private System.Windows.Forms.TextBox txtSecondKidsBDay;
        private System.Windows.Forms.GroupBox grpboxMarital;
        private System.Windows.Forms.GroupBox grpboxKidsEducationFirst;
        private System.Windows.Forms.GroupBox grpboxPartnersJob;
        private System.Windows.Forms.GroupBox grpboxKidsEducationSecond;
        private System.Windows.Forms.RadioButton rdbtnStudentSecond;
        private System.Windows.Forms.RadioButton rdbtnNotStudentSecond;
        private System.Windows.Forms.Label lblOtherLanguages2;
        private System.Windows.Forms.OpenFileDialog openFileUploadPhoto;
        private System.Windows.Forms.PictureBox picboxStaffAvatar;
        private System.Windows.Forms.Button btnUploadPhoto;
        private System.Windows.Forms.Label lblWorkType;
        private System.Windows.Forms.RadioButton rdbtnFull;
        private System.Windows.Forms.RadioButton rdbtnPart;
        private System.Windows.Forms.GroupBox grpbxWorkType;
        private System.Windows.Forms.MaskedTextBox txtSalary;
        private System.Windows.Forms.Label lblBMOSalary;
    }
}

