﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staff_Management1
{
    class StaffDatabase
    {
        public int id { get; set; } // Auto.
        public string name { get; set; } // User.
        public string surname { get; set; } // User.
        public int homeCity { get; set; } // User.
        public int workCity { get; set; } // User. 
        public int educationLevel { get; set; } // User.
        public int foreignLanguageKnowledgeLevel { get; set; } // User.
        public string[] knownOtherLanguages { get; set; } // User. // Default: -
        public int jobLevel { get; set; } // User.
        public string jobNotListed { get; set; } // User. // Default: -
        public string hiringDate { get; set; } // User.
        public int experienceAtHiringDate { get; set; } // User.
        public bool maritalStatus { get; set; } // User.
        public int partnersJobStatus { get; set; } // User. // -1 Not Married. 0 Not Working 1 Working.
        public int kidCountLevel { get; set; } // User.
        public string[] kidsBirthdays { get; set; } // User. // Default: -
        public int[] kidsEducationStatus { get; set; } // User. // -1 No Kid. 0 Not Student 1 Student.
        public double salary { get; set; } // User.
        public bool fullTime { get; set; } //User.
    }
}
