﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Staff_Management1
{
    public partial class MainForm : Form
    {
        StaffListControl staffListControl = new StaffListControl(multipleDbList);
        static List<Multiple> multipleDbList = new List<Multiple>(); // Multiple Database.

        public MainForm()
        {
            InitializeComponent();
        }

        // Database Auto Load And Multiple Database Auto Load - Start
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Database Load - Start
            int errorCheck = 0;
            string errorMessage = "";

            string path = @"C:\Users\Kartal\Desktop\Test.txt"; // Path for Auto Load at the Start of Program.

            if (File.Exists(path))
            {
                string loadRes = File.ReadAllText(path);
                loadRes = loadRes.Trim();

                if (loadRes != "")
                {
                    string[] lines = loadRes.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (int i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(',');

                        int idX = -1;
                        string nameX = "";
                        string surnameX = "";
                        string jobNotListedX = "-";
                        string hiringDateX = "";
                        string[] knownOtherLanguagesX = { "-" };
                        string[] kidsBirthdaysX = { "-" };
                        int homeCityX = -1;
                        int workCityX = -1;
                        int educationLevelX = -1;
                        int foreignLanguageKnowledgeLevelX = -1;
                        int jobLevelX = -1;
                        int experienceAtHiringDateX = -1;
                        int partnersJobStatusX = -1;
                        int kidCountLevelX = -1;
                        int[] kidsEducationStatusX = { -1 };
                        double salaryX = 0;
                        bool maritalStatusX = false;
                        bool fullTimeX = false;

                        // Id - [0]
                        try
                        {
                            idX = Int32.Parse(elements[0]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Name - [1]
                        try
                        {
                            nameX = elements[1];
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Surname - [2]
                        try
                        {
                            surnameX = elements[2];
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Home City - [3]
                        try
                        {
                            homeCityX = Int32.Parse(elements[3]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Work City - [4]
                        try
                        {
                            workCityX = Int32.Parse(elements[4]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Education Level - [5]
                        try
                        {
                            educationLevelX = Int32.Parse(elements[5]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Foreign Language Knowledge Level - [6]
                        try
                        {
                            foreignLanguageKnowledgeLevelX = Int32.Parse(elements[6]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Known Other Languages - [7]
                        try
                        {
                            string tmpknownOtherLanguages = elements[7];
                            knownOtherLanguagesX = tmpknownOtherLanguages.Split('/');
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Job Level - [8]
                        try
                        {
                            jobLevelX = Int32.Parse(elements[8]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Job Not Listed - [9]
                        try
                        {
                            jobNotListedX = elements[9];
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Hiring Date - [10]
                        try
                        {
                            hiringDateX = elements[10];
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Experience At Hiring Date - [11]
                        try
                        {
                            experienceAtHiringDateX = Int32.Parse(elements[11]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Marital Status - [12]
                        try
                        {
                            if (elements[12] != "False")
                            {
                                maritalStatusX = true;
                            }
                            else if(elements[12] != "True")
                            {
                                maritalStatusX = false;
                            }
                            else
                            {
                                errorCheck = -1;
                            }
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Partners Job Status - [13]
                        try
                        {
                            partnersJobStatusX = Int32.Parse(elements[13]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Kid Count Level - [14]
                        try
                        {
                            kidCountLevelX = Int32.Parse(elements[14]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Kids Birthdays - [15]
                        try
                        {
                            string tmpKidsBirthdays = elements[15];
                            kidsBirthdaysX = tmpKidsBirthdays.Split('/');
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Kids Education Status - [16]
                        try
                        {
                            string tmpKidsEducationStatus = elements[16];
                            kidsEducationStatusX = tmpKidsEducationStatus.Split('/').Select(Int32.Parse).ToArray();
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Salary - [17]
                        try
                        {
                            salaryX = Double.Parse(elements[17]);
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        // Work Type - [18]
                        try
                        {
                            if (elements[18] != "False")
                            {
                                fullTimeX = true;
                            }
                            else if (elements[18] != "True")
                            {
                                fullTimeX = false;
                            }
                            else
                            {
                                errorCheck = -1;
                            }
                        }
                        catch
                        {
                            errorCheck = -1;
                        }

                        if (errorCheck == -1)
                        {
                            errorMessage += "Load File Is Not Correct at " + i.ToString() + ". Index..." + Environment.NewLine;
                            errorCheck = 0;
                        }
                        else
                        {
                            staffListControl.StaffID = idX;

                            // Listview Staff Ekleme.
                            string[] staff = { idX.ToString(), nameX, surnameX, salaryX.ToString() };
                            ListViewItem listViewItem = new ListViewItem(staff);
                            lstviewStaff.Items.Add(listViewItem);

                            // Databese Staff Ekleme.
                            staffListControl.addStaff(nameX, surnameX, homeCityX, workCityX, educationLevelX, foreignLanguageKnowledgeLevelX, knownOtherLanguagesX
                                        , jobLevelX, jobNotListedX, hiringDateX, experienceAtHiringDateX, maritalStatusX, partnersJobStatusX, kidCountLevelX
                                        , kidsBirthdaysX, kidsEducationStatusX, salaryX, fullTimeX);
                        }
                    }
                    if(errorMessage!="")
                    {
                        MessageBox.Show(errorMessage);
                    }
                }
                else
                {
                    MessageBox.Show("Load File Is Empty...");
                }
            }
            // Database Load - End

            // Multiple Database Load - Start

            // Load Multiples Database - Start
            int errorCheckMultiple = 0;
            string pathMultiple = @"C:\Users\Kartal\Desktop\MultiplesTest.txt";

            if (File.Exists(pathMultiple))
            {
                string loadRes = File.ReadAllText(pathMultiple);
                loadRes = loadRes.Trim();

                if (loadRes != "")
                {
                    string[] lines = loadRes.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    double[] experienceX = { 0, 0, 0, 0, 0 };
                    double[] cityX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    double[] educationX = { 0, 0, 0, 0, 0 };
                    double[] foreignLanguageKnowledgeX = { 0 };
                    double[] jobX = { 0, 0, 0, 0, 0, 0 };
                    double[] familyStatusX = { 0, 0, 0, 0 };
                    double BMOSalaryX = 0;

                    // Experience
                    try
                    {
                        experienceX = lines[0].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    // City
                    try
                    {
                        cityX = lines[1].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    // Education
                    try
                    {
                        educationX = lines[2].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    // Foreign Language Knowledge
                    try
                    {
                        foreignLanguageKnowledgeX = lines[3].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    // Job
                    try
                    {
                        jobX = lines[4].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    // Family Status
                    try
                    {
                        familyStatusX = lines[5].Split('\t').Select(Double.Parse).ToArray();
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    // BMO Salary
                    try
                    {
                        BMOSalaryX = Double.Parse(lines[6]);
                    }
                    catch
                    {
                        errorCheckMultiple = -1;
                    }

                    if (errorCheckMultiple == 0)
                    {
                        multipleDbList.Add(new Multiple
                        {
                            experience = experienceX,
                            city = cityX,
                            education = educationX,
                            foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                            job = jobX,
                            familyStatus = familyStatusX,
                            BMOSalary = BMOSalaryX
                        });
                    }
                    else
                    {
                        multipleDbList.Add(new Multiple
                        {
                            experience = experienceX,
                            city = cityX,
                            education = educationX,
                            foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                            job = jobX,
                            familyStatus = familyStatusX,
                            BMOSalary = BMOSalaryX
                        });
                        MessageBox.Show("'Multiple' Load File Is Wrong...All Multiples Set To '0'");
                    }
                }
                else
                {
                    double[] experienceX = { 0, 0, 0, 0, 0 };
                    double[] cityX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    double[] educationX = { 0, 0, 0, 0, 0 };
                    double[] foreignLanguageKnowledgeX = { 0, 0, 0 };
                    double[] jobX = { 0, 0, 0, 0, 0, 0 };
                    double[] familyStatusX = { 0, 0, 0, 0 };
                    double BMOSalaryX = 0;

                    multipleDbList.Add(new Multiple
                    {
                        experience = experienceX,
                        city = cityX,
                        education = educationX,
                        foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                        job = jobX,
                        familyStatus = familyStatusX,
                        BMOSalary = BMOSalaryX
                    });
                    MessageBox.Show("'Multiple' Load File Is Empty...All Multiples Set To '0'");
                }
            }
            else // If File Doesnt Exists Set 0 as Default
            {
                double[] experienceX = { 0, 0, 0, 0, 0 };
                double[] cityX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                double[] educationX = { 0, 0, 0, 0, 0 };
                double[] foreignLanguageKnowledgeX = { 0, 0, 0 };
                double[] jobX = { 0, 0, 0, 0, 0, 0 };
                double[] familyStatusX = { 0, 0, 0, 0 };
                double BMOSalaryX = 0;

                multipleDbList.Add(new Multiple
                {
                    experience = experienceX,
                    city = cityX,
                    education = educationX,
                    foreignLanguageKnowledge = foreignLanguageKnowledgeX,
                    job = jobX,
                    familyStatus = familyStatusX,
                    BMOSalary = BMOSalaryX
                });
                MessageBox.Show("'Multiple' Load File Doesnt Exists...All Multiples Set To '0'");
            }
            // Load Multiples Database - End
        }
        // Database Auto Load And Multiple Database Auto Load - Start

        // Staff Details - Start
        private void lstviewStaff_DoubleClick(object sender, EventArgs e)
        {
            StaffManagementForm staffManagementForm = new StaffManagementForm(staffListControl, this);

            int index = lstviewStaff.SelectedIndices[0];
            int id = Int32.Parse(lstviewStaff.Items[index].SubItems[0].Text);

            int indexAtDB = staffListControl.indexOfStaffID(id);
            staffManagementForm.StaffIndex = indexAtDB;
            staffManagementForm.Show();
        }
        // Staff Details - End

        // Add New Staff / Open Add Staff Form - Start
        private void btnAddNewStaff_Click(object sender, EventArgs e)
        {
            StaffManagementForm staffManagementForm = new StaffManagementForm(staffListControl, this);
            staffManagementForm.Show();
        }
        // Add New Staff / Open Add Staff Form - End

        // Add Staff To Listview - Start
        public void addStaffToList(string staffX)
        {
            string[] staff = staffX.Split(',');
            ListViewItem listViewItem = new ListViewItem(staff);
            lstviewStaff.Items.Add(listViewItem);
        }
        // Add Staff To Listview - End

        // Update Staff From Listview - Start
        public void updateStaffFromList(int idX, string staffX)
        {
            int index = -1;
            for (int i = 0; i < lstviewStaff.Items.Count; i++)
            {
                ListViewItem item = lstviewStaff.Items[i];
                if (item.SubItems[0].Text == idX.ToString())
                {
                    index = i;
                }
            }
            if (index != -1)
            {
                string[] staff = staffX.Split(',');
                ListViewItem listViewItem = new ListViewItem(staff);
                lstviewStaff.Items[index] = listViewItem;
            }
        }
        // Update Staff From Listview - End

        // Delete Staff From Listview - Start
        public void deleteStaffFromList(int idX)
        {
            int index = -1;
            for (int i = 0; i < lstviewStaff.Items.Count; i++)
            {
                ListViewItem item = lstviewStaff.Items[i];
                if (item.SubItems[0].Text == idX.ToString())
                {
                    index = i;
                }
            }
            if (index != -1)
            {
                lstviewStaff.Items.RemoveAt(index);
            }
        }
        // Delete Staff From Listview - End

        // Database Save - Start
        private void btnSave_Click(object sender, EventArgs e)
        {
            string saveRes = "";

            savefileStaffList.Filter = "CSV Dosyası|*.txt|TSV Dosyası|*.txt|JSON Dosyası|*.txt";

            if (savefileStaffList.ShowDialog() == DialogResult.OK)
            {
                bool tsvCsv = false;
                if (savefileStaffList.FilterIndex == 1)
                {
                    tsvCsv = true;
                    saveRes = staffListControl.listThemAll(",");
                }
                else if (savefileStaffList.FilterIndex == 2)
                {
                    tsvCsv = true;
                    saveRes = staffListControl.listThemAll("\t");
                }
                if (tsvCsv)
                {
                    StreamWriter streamWriter = new StreamWriter(savefileStaffList.FileName);
                    streamWriter.WriteLine(saveRes);
                    streamWriter.Close();
                    return;
                }
                if (savefileStaffList.FilterIndex == 3)
                {
                    System.IO.File.WriteAllText(savefileStaffList.FileName, staffListControl.jsonString());
                }
            }
        }
        // Database Save - End

        // Database Load - Start
        private void btnLoad_Click(object sender, EventArgs e)
        {
            int errorCheck = 0;
            string errorMessage = "";

            string loadRes = "";
            char x = ',';
            int tsvCsv = -1; // 0 = CSV / TSV --- 1 = JSON --- -1 = Error.

            openfileStaffList.Filter = "CSV - TSV - JSON Dosyası|*.txt";

            if (openfileStaffList.ShowDialog() == DialogResult.OK)
            {
                // Progressbarr Define Begin.
                ProgressBar pBar = new ProgressBar();
                pBar.Location = new System.Drawing.Point(20, 20);
                pBar.Name = "progressBar1";
                pBar.Width = 200;
                pBar.Height = 10;
                Controls.Add(pBar);
                pBar.Name = "progbarLoad";
                pBar.Dock = DockStyle.Bottom;
                pBar.Minimum = 0;
                pBar.Maximum = 100;
                // Progressbar Define End.

                StreamReader streamReader = new StreamReader(openfileStaffList.FileName);
                loadRes += streamReader.ReadToEnd();
                streamReader.Close();

                loadRes = loadRes.Trim();
                pBar.Value = 20;

                if (loadRes != "")
                {
                    lstviewStaff.Items.Clear();
                    staffListControl.clearTheList();

                    if (loadRes.Contains(",") && !loadRes.Contains("{"))
                    {
                        x = ',';
                        tsvCsv = 0;
                    }
                    else if (loadRes.Contains("\t"))
                    {
                        x = '\t';
                        tsvCsv = 0;
                    }
                    else if (loadRes.Contains("{"))
                    {
                        tsvCsv = 1;
                    }

                    pBar.Value = 50;


                    if (tsvCsv == 0)
                    {
                        string[] lines = loadRes.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                        for (int i = 0; i < lines.Length; i++)
                        {
                            string[] elements = lines[i].Split(x);

                            int idX = -1;
                            string nameX = "";
                            string surnameX = "";
                            string jobNotListedX = "-";
                            string hiringDateX = "";
                            string[] knownOtherLanguagesX = { "-" };
                            string[] kidsBirthdaysX = { "-" };
                            int homeCityX = -1;
                            int workCityX = -1;
                            int educationLevelX = -1;
                            int foreignLanguageKnowledgeLevelX = -1;
                            int jobLevelX = -1;
                            int experienceAtHiringDateX = -1;
                            int partnersJobStatusX = -1;
                            int kidCountLevelX = -1;
                            int[] kidsEducationStatusX = { -1 };
                            double salaryX = 0;
                            bool maritalStatusX = false;
                            bool fullTimeX = false;

                            // Id - [0]
                            try
                            {
                                idX = Int32.Parse(elements[0]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Name - [1]
                            try
                            {
                                nameX = elements[1];
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Surname - [2]
                            try
                            {
                                surnameX = elements[2];
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Home City - [3]
                            try
                            {
                                homeCityX = Int32.Parse(elements[3]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Work City - [4]
                            try
                            {
                                workCityX = Int32.Parse(elements[4]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Education Level - [5]
                            try
                            {
                                educationLevelX = Int32.Parse(elements[5]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Foreign Language Knowledge Level - [6]
                            try
                            {
                                foreignLanguageKnowledgeLevelX = Int32.Parse(elements[6]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Known Other Languages - [7]
                            try
                            {
                                string tmpknownOtherLanguages = elements[7];
                                knownOtherLanguagesX = tmpknownOtherLanguages.Split('/');
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Job Level - [8]
                            try
                            {
                                jobLevelX = Int32.Parse(elements[8]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Job Not Listed - [9]
                            try
                            {
                                jobNotListedX = elements[9];
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Hiring Date - [10]
                            try
                            {
                                hiringDateX = elements[10];
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Experience At Hiring Date - [11]
                            try
                            {
                                experienceAtHiringDateX = Int32.Parse(elements[11]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Marital Status - [12]
                            try
                            {
                                if (elements[12] != "False")
                                {
                                    maritalStatusX = true;
                                }
                                else if (elements[12] != "True")
                                {
                                    maritalStatusX = false;
                                }
                                else
                                {
                                    errorCheck = -1;
                                }
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Partners Job Status - [13]
                            try
                            {
                                partnersJobStatusX = Int32.Parse(elements[13]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Kid Count Level - [14]
                            try
                            {
                                kidCountLevelX = Int32.Parse(elements[14]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Kids Birthdays - [15]
                            try
                            {
                                string tmpKidsBirthdays = elements[15];
                                kidsBirthdaysX = tmpKidsBirthdays.Split('/');
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Kids Education Status - [16]
                            try
                            {
                                string tmpKidsEducationStatus = elements[16];
                                kidsEducationStatusX = tmpKidsEducationStatus.Split('/').Select(Int32.Parse).ToArray();
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Salary - [17]
                            try
                            {
                                salaryX = Double.Parse(elements[17]);
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            // Work Type - [18]
                            try
                            {
                                if (elements[18] != "False")
                                {
                                    fullTimeX = true;
                                }
                                else if (elements[18] != "True")
                                {
                                    fullTimeX = false;
                                }
                                else
                                {
                                    errorCheck = -1;
                                }
                            }
                            catch
                            {
                                errorCheck = -1;
                            }

                            if (errorCheck == -1)
                            {
                                errorMessage += "Load File Is Not Correct at " + i.ToString() + ". Index..." + Environment.NewLine;
                                errorCheck = 0;
                            }
                            else
                            {
                                staffListControl.StaffID = idX;

                                // Listview Staff Ekleme.
                                string[] staff = { idX.ToString(), nameX, surnameX, salaryX.ToString() };
                                ListViewItem listViewItem = new ListViewItem(staff);
                                lstviewStaff.Items.Add(listViewItem);

                                // Databese Staff Ekleme.
                                staffListControl.addStaff(nameX, surnameX, homeCityX, workCityX, educationLevelX, foreignLanguageKnowledgeLevelX, knownOtherLanguagesX
                                            , jobLevelX, jobNotListedX, hiringDateX, experienceAtHiringDateX, maritalStatusX, partnersJobStatusX, kidCountLevelX
                                            , kidsBirthdaysX, kidsEducationStatusX, salaryX, fullTimeX);
                            }
                        }
                        if (errorMessage != "")
                        {
                            MessageBox.Show(errorMessage);
                        }
                        pBar.Value = 100;
                        pBar.Dispose();
                        return;
                    }
                    else if (tsvCsv == 1)
                    {
                        try
                        {
                            staffListControl.jsonReading(openfileStaffList.FileName);
                            for (int i = 0; i < staffListControl.staffCount(); i++)
                            {
                                ListViewItem listViewItem = new ListViewItem(staffListControl.staffForList(i));
                                lstviewStaff.Items.Add(listViewItem);
                            }
                        }
                        catch
                        {
                            errorMessage = "JSON Load Failed...";
                            MessageBox.Show(errorMessage);
                        }
                        pBar.Value = 100;
                        pBar.Dispose();
                    }
                    else
                    {
                        errorMessage = "Load File Is Not Correct...";
                        MessageBox.Show(errorMessage);
                        pBar.Value = 100;
                        pBar.Dispose();
                    }
                }
                else
                {
                    MessageBox.Show("Load File Is Empty...");
                    pBar.Value = 100;
                    pBar.Dispose();   
                }
            }
        }
        // Database Load - End

        // Open Multiple Editor - Start
        private void btnMultipleEditor_Click(object sender, EventArgs e)
        {
            MultipleEditorForm multipleEditorForm = new MultipleEditorForm(multipleDbList);
            multipleEditorForm.Show();
        }
        // Open Multiple Editor - Start

        // Added For Sorting... - Start
        static int ascending = -1;
        static int selectedColumn = -1;
        private void lstviewStaff_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ascending == 1)
            { ascending = -1; }
            else
            { ascending = 1; }

            selectedColumn = e.Column;
            string[] tempStr = new string[lstviewStaff.Items.Count];
            for (int i = 0; i < lstviewStaff.Items.Count; i++)
            {
                tempStr[i] = lstviewStaff.Items[i].SubItems[e.Column].Text;
            }
            int[] array = new int[lstviewStaff.Items.Count];
            for (int i = 0; i < lstviewStaff.Items.Count; i++)
            {
                array[i] = i;
            }
            array = mergeSort(array, tempStr);
            ListView tempList = new ListView();
            ListViewItem tempItem;
            for (int i = 0; i < tempStr.Length; i++)
            {
                tempItem = new ListViewItem(new string[] { lstviewStaff.Items[i].SubItems[0].Text, lstviewStaff.Items[i].SubItems[1].Text, lstviewStaff.Items[i].SubItems[2].Text, lstviewStaff.Items[i].SubItems[3].Text });
                tempList.Items.Add(tempItem);
            }
            lstviewStaff.Items.Clear();
            for (int i = 0; i < tempStr.Length; i++)
            {
                tempItem = new ListViewItem(new string[] { tempList.Items[array[i]].SubItems[0].Text, tempList.Items[array[i]].SubItems[1].Text, tempList.Items[array[i]].SubItems[2].Text, tempList.Items[array[i]].SubItems[3].Text });
                lstviewStaff.Items.Add(tempItem);
            }
        }
        public static int[] mergeSort(int[] array, string[] str)
        {
            int[] left;
            int[] right;
            int[] result = new int[array.Length];

            if (array.Length <= 1)
            { return array; }

            int midPoint = array.Length / 2;
            left = new int[midPoint];

            if (array.Length % 2 == 0)
            { right = new int[midPoint]; }
            else
            { right = new int[midPoint + 1]; }

            for (int i = 0; i < midPoint; i++)
            {
                left[i] = array[i];
            }

            int x = 0;
            for (int i = midPoint; i < array.Length; i++)
            {
                right[x] = array[i];
                x++;
            }
            left = mergeSort(left, str);
            right = mergeSort(right, str);
            result = merge(left, right, str);
            return result;
        }
        public static int[] merge(int[] left, int[] right, string[] str)
        {
            int resultLength = right.Length + left.Length;
            int[] result = new int[resultLength];
            int indexLeft = 0, indexRight = 0, indexResult = 0;
            while (indexLeft < left.Length || indexRight < right.Length)
            {
                if (indexLeft < left.Length && indexRight < right.Length)
                {
                    int comp = -1;
                    if (selectedColumn == 3)
                    {
                        if (Double.Parse(str[left[indexLeft]]) > Double.Parse(str[right[indexRight]]))
                        {
                            comp = 1;
                        }
                        else if (Double.Parse(str[left[indexLeft]]) == Double.Parse(str[right[indexRight]]))
                        {
                            comp = 0;
                        }
                        else
                        {
                            comp = -1;
                        }
                    }
                    else
                    {
                        comp = str[left[indexLeft]].CompareTo(str[right[indexRight]]);
                    }

                    if (comp != ascending)
                    {
                        result[indexResult] = left[indexLeft];
                        indexLeft++;
                        indexResult++;
                    }
                    else
                    {
                        result[indexResult] = right[indexRight];
                        indexRight++;
                        indexResult++;
                    }
                }
                else if (indexLeft < left.Length)
                {
                    result[indexResult] = left[indexLeft];
                    indexLeft++;
                    indexResult++;
                }
                else if (indexRight < right.Length)
                {
                    result[indexResult] = right[indexRight];
                    indexRight++;
                    indexResult++;
                }
            }
            return result;
        }
        // Added For Sorting... - End

        // Form Closing Event - Start
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Do you really want to exit?", "Exit", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            { Application.Exit(); }
            else
            { e.Cancel = true; }
        }
        // Form Closing Event - End  
    }
}