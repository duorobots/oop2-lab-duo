﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Staff_Management1
{
    public class StaffListControl
    { 
        List<StaffDatabase> staffDbList = new List<StaffDatabase>(); // Database.
        int staffID = 1; // staffID starting value.
        List<Multiple> multipleDbList;

        // Overloaded Constructor to Get Address of Created Objects from MainForm - Start
        public StaffListControl(List<Multiple> x)
        {
            multipleDbList = x;
        }
        // Overloaded Constructor to Get Address of Created Objects from MainForm - End

        // StaffID Get/Set - Start
        public int StaffID
        {
            get { return staffID; }
            set { staffID = value; }
        }
        // StaffID Get/Set - End

        // Return ID of Selected Index - Start
        public int staffIDofSelectedIndex(int indexX)
        {
            return staffDbList[indexX].id;
        }
        // Return ID of Selected Index - End

        // Return Index of Selected Staff ID - Start
        public int indexOfStaffID(int idX)
        {
            int index = -1;

            for (int i = 0; i < staffDbList.Count; i++)
            {
                if (staffDbList[i].id == idX)
                {
                    index = i;
                }
            }
            return index;
        }
        // Return Index of Selected Staff ID - End

        // Add Staff to Database - Start
        public void addStaff(string nameX, string surnameX, int homeCityX, int workCityX, int educationLevelX, int foreignLanguageKnowledgeLevelX, string[] knownOtherLanguagesX
                            , int jobLevelX, string jobNotListedX, string hiringDateX, int experienceAtHiringDateX, bool maritalStatusX, int partnersJobStatusX, int kidCountLevelX
                            , string[] kidsBirthdaysX, int[] kidsEducationStatusX, double salaryX, bool fullTimeX)
        {
            staffDbList.Add(new StaffDatabase
            {
                id = staffID,
                name = nameX,
                surname = surnameX,
                homeCity = homeCityX,
                workCity = workCityX,
                educationLevel = educationLevelX,
                foreignLanguageKnowledgeLevel = foreignLanguageKnowledgeLevelX,
                knownOtherLanguages = knownOtherLanguagesX,
                jobLevel = jobLevelX,
                jobNotListed = jobNotListedX,
                hiringDate = hiringDateX,
                experienceAtHiringDate = experienceAtHiringDateX,
                maritalStatus = maritalStatusX,
                partnersJobStatus = partnersJobStatusX,
                kidCountLevel = kidCountLevelX,
                kidsBirthdays = kidsBirthdaysX,
                kidsEducationStatus = kidsEducationStatusX,
                salary = salaryX,
                fullTime = fullTimeX
            });
            staffID++;
        }
        // Add Staff to Database - End

        // Update Staff from Database - Start
        public void updateStaff(int indexX, string nameX, string surnameX, int homeCityX, int workCityX, int educationLevelX, int foreignLanguageKnowledgeLevelX
                            , string[] knownOtherLanguagesX, int jobLevelX, string jobNotListedX, string hiringDateX, int experienceAtHiringDateX, bool maritalStatusX
                            , int partnersJobStatusX, int kidCountLevelX, string[] kidsBirthdaysX, int[] kidsEducationStatusX, double salaryX , bool fullTimeX)
        {
            staffDbList[indexX].name = nameX;
            staffDbList[indexX].surname = surnameX;
            staffDbList[indexX].homeCity = homeCityX;
            staffDbList[indexX].workCity = workCityX;
            staffDbList[indexX].educationLevel = educationLevelX;
            staffDbList[indexX].foreignLanguageKnowledgeLevel = foreignLanguageKnowledgeLevelX;
            staffDbList[indexX].knownOtherLanguages = knownOtherLanguagesX;
            staffDbList[indexX].jobLevel = jobLevelX;
            staffDbList[indexX].jobNotListed = jobNotListedX;
            staffDbList[indexX].hiringDate = hiringDateX;
            staffDbList[indexX].experienceAtHiringDate = experienceAtHiringDateX;
            staffDbList[indexX].maritalStatus = maritalStatusX;
            staffDbList[indexX].partnersJobStatus = partnersJobStatusX;
            staffDbList[indexX].kidCountLevel = kidCountLevelX;
            staffDbList[indexX].kidsBirthdays = kidsBirthdaysX;
            staffDbList[indexX].kidsEducationStatus = kidsEducationStatusX;
            staffDbList[indexX].salary = salaryX;
            staffDbList[indexX].fullTime = fullTimeX;
        }
        // Update Staff from Database - End

        // Delete Staff from Database - Start
        public void deleteStaff(int indexX)
        {
            staffDbList.RemoveAt(indexX);
        }
        // Delete Staff from Database - End

        // List All Staffs of Database - Start
        public string listThemAll(string x)
        {
            string res = "";

            for (int i = 0; i < staffDbList.Count; i++)
            {
                res += staffDbList[i].id.ToString() + x;
                res += staffDbList[i].name + x;
                res += staffDbList[i].surname + x;
                res += staffDbList[i].homeCity.ToString() + x;
                res += staffDbList[i].workCity.ToString() + x;
                res += staffDbList[i].educationLevel.ToString() + x;
                res += staffDbList[i].foreignLanguageKnowledgeLevel.ToString() + x;

                for (int j = 0; j < staffDbList[i].knownOtherLanguages.Length; j++)
                {
                    res += staffDbList[i].knownOtherLanguages[j] + "/";
                }
                res = res.Remove(res.Length - 1);
                res += x;

                res += staffDbList[i].jobLevel.ToString() + x;
                res += staffDbList[i].jobNotListed + x;
                res += staffDbList[i].hiringDate + x;
                res += staffDbList[i].experienceAtHiringDate.ToString() + x;
                res += staffDbList[i].maritalStatus + x;
                res += staffDbList[i].partnersJobStatus.ToString() + x;
                res += staffDbList[i].kidCountLevel.ToString() + x;

                for (int j = 0; j < staffDbList[i].kidsBirthdays.Length; j++)
                {
                    res += staffDbList[i].kidsBirthdays[j] + "/";
                }
                res = res.Remove(res.Length - 1);
                res += x;

                for (int j = 0; j < staffDbList[i].kidsEducationStatus.Length; j++)
                {
                    res += staffDbList[i].kidsEducationStatus[j] + "/";
                }
                res = res.Remove(res.Length - 1);
                res += x;

                res += staffDbList[i].salary.ToString() + x;
                res += staffDbList[i].fullTime + Environment.NewLine;
            }
            return res;
        }
        // List All Staffs of Database - End

        // Clear Database - Start
        public void clearTheList()
        {
            staffDbList.Clear();
            staffID = 1;
        }
        // Clear Database - End

        // List Selected Staff Infos - Start
        public string bringTheStaff(int indexX)
        {
            string res = "";

            res += staffDbList[indexX].id.ToString() + ",";
            res += staffDbList[indexX].name + ",";
            res += staffDbList[indexX].surname + ",";
            res += staffDbList[indexX].homeCity.ToString() + ",";
            res += staffDbList[indexX].workCity.ToString() + ",";
            res += staffDbList[indexX].educationLevel.ToString() + ",";
            res += staffDbList[indexX].foreignLanguageKnowledgeLevel.ToString() + ",";

            for (int j = 0; j < staffDbList[indexX].knownOtherLanguages.Length; j++)
            {
                res += staffDbList[indexX].knownOtherLanguages[j] + "/";
            }
            res = res.Remove(res.Length - 1);
            res += ",";

            res += staffDbList[indexX].jobLevel.ToString() + ",";
            res += staffDbList[indexX].jobNotListed + ",";
            res += staffDbList[indexX].hiringDate + ",";
            res += staffDbList[indexX].experienceAtHiringDate.ToString() + ",";
            res += staffDbList[indexX].maritalStatus + ",";
            res += staffDbList[indexX].partnersJobStatus.ToString() + ",";
            res += staffDbList[indexX].kidCountLevel.ToString() + ",";

            for (int j = 0; j < staffDbList[indexX].kidsBirthdays.Length; j++)
            {
                res += staffDbList[indexX].kidsBirthdays[j] + "/";
            }
            res = res.Remove(res.Length - 1);
            res += ",";

            for (int j = 0; j < staffDbList[indexX].kidsEducationStatus.Length; j++)
            {
                res += staffDbList[indexX].kidsEducationStatus[j] + "/";
            }
            res = res.Remove(res.Length - 1);
            res += ",";

            res += staffDbList[indexX].salary.ToString() + ",";
            res += staffDbList[indexX].fullTime + Environment.NewLine;

            return res;
        }
        // List Selected Staff Infos - End

        // Calculate BMO Salary of Selected Staff - Start
        public double calculateBMO(int indexX)
        {
            double BMOPoint = 0;
            double BMOSalary = 0;

            // Experience Point - Start
            int tmpHD = Int32.Parse(staffDbList[indexX].hiringDate);
            int tmpEAHD = staffDbList[indexX].experienceAtHiringDate;
            int tmpDate = Int32.Parse(DateTime.UtcNow.ToString("yyyy"));

            int experience = tmpDate - tmpHD + tmpEAHD;

            if (experience >= 2 && experience <= 4)
            {
                BMOPoint += multipleDbList[0].experience[0];
            }
            else if (experience >= 5 && experience <= 9)
            {
                BMOPoint += multipleDbList[0].experience[1];
            }
            else if (experience >= 10 && experience <= 14)
            {
                BMOPoint += multipleDbList[0].experience[2];
            }
            else if (experience >= 15 && experience <= 20)
            {
                BMOPoint += multipleDbList[0].experience[3];
            }
            else if (experience > 20)
            {
                BMOPoint += multipleDbList[0].experience[4];
            }
            // Experience Point - End

            // City Point - Start
            int tmpHC = staffDbList[indexX].homeCity;
            double tmpBMOHC = 0;

            int tmpWC = staffDbList[indexX].workCity;
            double tmpBMOWC = 0;

            tmpBMOHC = multipleDbList[0].city[tmpHC];
            tmpBMOWC = multipleDbList[0].city[tmpWC];
 
            if (tmpBMOHC >= tmpBMOWC)
            {
                BMOPoint += tmpBMOHC;
            }
            else
            {
                BMOPoint += tmpBMOWC;
            }
            // City Point - End

            // Education Point - Start
            int tmpEducationLevel = staffDbList[indexX].educationLevel;

            if (tmpEducationLevel == 3)
            {
                BMOPoint += multipleDbList[0].education[0];
            }
            else if (tmpEducationLevel == 4)
            {
                BMOPoint += multipleDbList[0].education[1];
            }
            else if (tmpEducationLevel == 5)
            {
                BMOPoint += multipleDbList[0].education[2];
            }
            else if (tmpEducationLevel == 6)
            {
                BMOPoint += multipleDbList[0].education[3];
            }
            else if (tmpEducationLevel == 7)
            {
                BMOPoint += multipleDbList[0].education[4];
            }
            // Education Point - End

            // Language Point - Start
            int tmpLanguageLevel = staffDbList[indexX].foreignLanguageKnowledgeLevel;
            int tmpCountOtherLanguages = 0;

            if (staffDbList[indexX].knownOtherLanguages[0] != "-")
            {
                tmpCountOtherLanguages = staffDbList[indexX].knownOtherLanguages.Length;
            }

            if (tmpLanguageLevel == 1)
            {
                BMOPoint += multipleDbList[0].foreignLanguageKnowledge[0];
            }
            else if(tmpLanguageLevel == 2)
            {
                BMOPoint += multipleDbList[0].foreignLanguageKnowledge[1];
            }

            BMOPoint += tmpCountOtherLanguages * multipleDbList[0].foreignLanguageKnowledge[2];
            // Language Point - End

            // Job Point - Start
            int tmpJobLevel = staffDbList[indexX].jobLevel;

            int countLevel4s = 0;
            for (int i = 0; i < staffDbList.Count; i++)
            {
                if (staffDbList[i].jobLevel == 4)
                {
                    countLevel4s++;
                }
            }

            if (tmpJobLevel == 0)
            {
                BMOPoint += multipleDbList[0].job[0];
            }
            else if (tmpJobLevel == 1)
            {
                BMOPoint += multipleDbList[0].job[1];
            }
            else if (tmpJobLevel == 2)
            {
                BMOPoint += multipleDbList[0].job[2];
            }
            else if (tmpJobLevel == 3)
            {
                BMOPoint += multipleDbList[0].job[3];
            }
            else if (tmpJobLevel == 4)
            {
                if (countLevel4s <= 5)
                {
                    BMOPoint += multipleDbList[0].job[4];
                }
                else
                {
                    BMOPoint += multipleDbList[0].job[5];
                }
            }
            // Job Point - End

            // Family Point - Start
            bool tmpMarital = staffDbList[indexX].maritalStatus;
            int tmpPartnersJobStatus = staffDbList[indexX].partnersJobStatus;

            string tmpToday = DateTime.UtcNow.ToShortDateString();
            string[] today = tmpToday.Split('.');
            int tmpTodayD = Int32.Parse(today[0]);
            int tmpTodayM = Int32.Parse(today[1]);
            int tmpTodayY = Int32.Parse(today[2]);

            int firstAge = -1;
            int secondAge = -1;
            int tmpFirstEduStat = -1;
            int tmpSecondEduStat = -1;

            if (staffDbList[indexX].kidsBirthdays[0] != "-")
            {
                if (staffDbList[indexX].kidsBirthdays.Length > 1)
                {
                    // First Kid
                    string[] tmpFirst = staffDbList[indexX].kidsBirthdays[0].Split('.');
                    int tmpBDD = Int32.Parse(tmpFirst[0]);
                    int tmpBDM = Int32.Parse(tmpFirst[1]);
                    int tmpBDY = Int32.Parse(tmpFirst[2]);

                    firstAge = tmpTodayY - tmpBDY;
                    if (tmpBDM > tmpTodayM || (tmpBDM == tmpTodayM && tmpBDD > tmpTodayD))
                    {
                        firstAge--;
                    }

                    // Second Kid
                    string[] tmpSecond = staffDbList[indexX].kidsBirthdays[1].Split('.');
                    tmpBDD = Int32.Parse(tmpSecond[0]);
                    tmpBDM = Int32.Parse(tmpSecond[1]);
                    tmpBDY = Int32.Parse(tmpSecond[2]);

                    secondAge = tmpTodayY - tmpBDY;
                    if (tmpBDM > tmpTodayM || (tmpBDM == tmpTodayM && tmpBDD > tmpTodayD))
                    {
                        secondAge--;
                    }
                }
                else
                {
                    // First Kid
                    string[] tmpFirst = staffDbList[indexX].kidsBirthdays[0].Split('.');
                    int tmpBDD = Int32.Parse(tmpFirst[0]);
                    int tmpBDM = Int32.Parse(tmpFirst[1]);
                    int tmpBDY = Int32.Parse(tmpFirst[2]);

                    firstAge = tmpTodayY - tmpBDY;
                    if (tmpBDM > tmpTodayM || (tmpBDM == tmpTodayM && tmpBDD > tmpTodayD))
                    {
                        firstAge--;
                    }
                }
            }

            if (staffDbList[indexX].kidsEducationStatus[0] != -1)
            {
                if (staffDbList[indexX].kidsEducationStatus.Length > 1)
                {
                    tmpFirstEduStat = staffDbList[indexX].kidsEducationStatus[0];
                    tmpSecondEduStat = staffDbList[indexX].kidsEducationStatus[1];
                }
                else
                {
                    tmpFirstEduStat = staffDbList[indexX].kidsEducationStatus[0];
                }
            }

            if (tmpMarital != false && tmpPartnersJobStatus != 1)
            {
                BMOPoint += multipleDbList[0].familyStatus[0];
            }

            if (firstAge != -1)
            {
                if (firstAge > 18 && tmpFirstEduStat == 1)
                {
                    BMOPoint += multipleDbList[0].familyStatus[3];
                }
                else if (firstAge >= 7 && firstAge <= 18)
                {
                    BMOPoint += multipleDbList[0].familyStatus[2];
                }
                else if (firstAge >= 0 && firstAge <= 6)
                {
                    BMOPoint += multipleDbList[0].familyStatus[1];
                }
            }

            if (secondAge != -1)
            {
                if (secondAge > 18 && tmpSecondEduStat == 1)
                {
                    BMOPoint += multipleDbList[0].familyStatus[3];
                }
                else if (secondAge >= 7 && secondAge <= 18)
                {
                    BMOPoint += multipleDbList[0].familyStatus[2];
                }
                else if (secondAge >= 0 && secondAge <= 6)
                {
                    BMOPoint += multipleDbList[0].familyStatus[1];
                }
            }
            // Family Point - End

            BMOPoint += 1;
            if (!staffDbList[indexX].fullTime)
            { BMOPoint /= 2; }

            BMOSalary = BMOPoint * multipleDbList[0].BMOSalary;

            return BMOSalary;
        }
        // Calculate BMO Salary of Selected Staff - Start
        
        // Added For Json Parsing... - Start
        public string jsonString()
        {
            string json = JsonConvert.SerializeObject(staffDbList.ToArray());
            return json;
        }

        public void jsonReading(string fileName)
        {
            StreamReader file = new StreamReader(fileName);
            string json = file.ReadToEnd();
            staffDbList = JsonConvert.DeserializeObject<List<StaffDatabase>>(json);
            file.Close();
        }

        public int staffCount()
        {
            return staffDbList.Count;
        }

        public string[] staffForList(int ind)
        {
            string[] staff = { staffDbList[ind].id.ToString(), staffDbList[ind].name, staffDbList[ind].surname, staffDbList[ind].salary.ToString() };
            return staff;
        }
        // Added For Json Parsing... - End
    }
}